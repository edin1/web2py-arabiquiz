import os
import sys
import shutil

def setup_empty_db():
    src = os.path.join(os.path.abspath(os.curdir), "tests/databases_empty")
    dst = os.path.join(os.path.abspath(os.curdir), "applications/arabictest/databases")
    rm_db()
    shutil.copytree(src, dst)

def setup_one_user_11_passed_tests_db():
    src = os.path.join(os.path.abspath(os.curdir), "tests/databases_one_user_11_passed_tests")
    dst = os.path.join(os.path.abspath(os.curdir), "applications/arabictest/databases")
    rm_db()
    shutil.copytree(src, dst)

def rm_db():
    dst = os.path.join(os.path.abspath(os.curdir), "applications/arabictest/databases")
    try:
        shutil.rmtree(dst)
    except OSError:
        pass

if __name__ == "__main__":
    eval("%s()"%sys.argv[1])
