import os
import sys
import signal
import subprocess
import time

import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import RegexMatchingEventHandler
from watchdog.tricks import Trick
from watchdog.utils import echo, has_attribute

from pprint import pprint

class AutoRestartTrick(RegexMatchingEventHandler):
  """Starts a long-running subprocess and restarts it on matched events.

  The command parameter is a list of command arguments, such as
  ['bin/myserver', '-c', 'etc/myconfig.ini'].

  Call start() after creating the Trick. Call stop() when stopping
  the process.
  """

  def __init__(self, command, patterns=None, ignore_patterns=None,
               ignore_directories=False, stop_signal=signal.SIGINT,
               kill_after=10):
    super(AutoRestartTrick, self).__init__(
      patterns, ignore_patterns, ignore_directories)
    self.command = command
    self.stop_signal = stop_signal
    self.kill_after = kill_after
    self.processes = []
    self.start()

  def start(self):
    self.processes.append(subprocess.Popen("python removepyc.py".split()))
    self.processes.append(subprocess.Popen("python web2py.py -S arabictest -M -R scripts/sessions2trash.py -A -o -x 0".split()))
    self.processes.append(subprocess.Popen(self.command))#, bufsize=1), stdout=subprocess.PIPE)

  def stop(self):
    if not self.processes:
      return
    for process in self.processes:
      try:
        #self.process.send_signal(self.stop_signal)
        process.terminate()
      except OSError:
        # Process is already gone
        pass
      else:
        kill_time = time.time() + self.kill_after
        while time.time() < kill_time:
          if process.poll() is not None:
            break
          time.sleep(0.25)
        else:
          try:
            process.kill()
          except OSError:
            # Process is already gone
            pass
    self.processes = []

  @echo.echo
  def on_any_event(self, event):
    if not (event.src_path.endswith("parameters_8000.py") or event.src_path.find("languages") != -1):
      self.stop()
      self.start()


class MyHandler(AutoRestartTrick):

    def on_modified(self, event):
        file =  event.src_path
        if not file.endswith(".py"): return
        #else
        file += "c"
        if os.path.isfile(file):
            pprint('Removing: "%s"'%file)
            os.remove(file)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    #event_handler = LoggingEventHandler()
    event_handler = MyHandler(command=["python", "web2py.py", "-i0.0.0.0", "-aedin"],
                              patterns=[r".*\.py$"], ignore_patterns=[r".*parameters_8000\.py$"])
    # event_handler.start()
    # while event_handler.process.poll() is None:
    #   line = event_handler.process.stdout.readline()
    #   if line:
    #     # Process output here
    #     print 'Read line', line

    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
