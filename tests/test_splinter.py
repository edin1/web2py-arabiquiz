import os
import sys
import ast
import re
from pprint import pprint
import shutil

from splinter import Browser

from test_base import get_answers

sys.path.append('../applications/arabictest/modules/')
import mycrypto

browser = Browser()

scheme_domain_port = "http://localhost:8000"
#scheme_domain_port = "http://arabic-test.appspot.com/"

def test_databases_one_user_11_passed_tests():
    login("edin1.business@gmail.com", "ulazim1")
    url = scheme_domain_port + "/arabictest/en-us/sarfi/test/verbs/past/passiveneg"
    # pass_correctly(url)
    pass_with_mistakes(url, number_of_mistakes=3)

def test_no_cookies():
    browser.visit(scheme_domain_port)

def login(email, password):
    browser.visit(scheme_domain_port + "/arabictest/en-us/user/login")
    browser.find_by_id("auth_user_email").fill(email)
    browser.find_by_id("auth_user_password").fill(password)
    browser.find_by_id("auth_user_remember").check()
    browser.find_by_value("Login").click()

def pass_with_mistakes(url, number_of_mistakes=1):
    n = number_of_mistakes

    while True:
        browser.visit(url)
        _codetext_inputs = browser.find_by_name("codetext")
        if not _codetext_inputs:
            print "Final answer"
            break
        # else:
        codetext = _codetext_inputs[0].value
        index = mycrypto.w2p_decrypt(codetext.encode("utf-8"))
        # # Introduce mistakes

        # This introduces mistakes by removing correct checks
        if n > 0:
            for i in browser.find_by_tag("input"):
                if i.value != codetext and \
                    i.outer_html != browser.find_by_name(index).outer_html:
                    i.click()
                    n -= 1
                    break
        if n > 0:
            # We have more mistakes to introduce
            continue
        browser.find_by_name(index).click()


def pass_correctly(url):
    answers = get_answers(url)
    browser.visit(url)

    while True:
        inputs = browser.find_by_name("codetext")
        if not inputs:
            print "Final answer"
            break
        #else:
        codetext = inputs[0].value.encode("utf-8")
        index = mycrypto.w2p_decrypt(codetext)
        browser.find_by_name(index).click()
        html = browser.html
        remaining = re.search(r'([0-9]+)</span>\s*remaining', html, re.DOTALL).group(1).strip()
        if remaining == "1":
            break

# browser.fill('q', 'splinter - python acceptance testing for web applications')
# # Find and click the 'search' button
# button = browser.find_by_name('btnK')
# # Interact with elements
# button.click()
# if browser.is_text_present('splinter.cobrateam.info'):
#     print "Yes, the official website was found!"
# else:
#     print "No, it wasn't found... We need to improve our SEO techniques"

if __name__ == "__main__":
    from optparse import OptionParser

    cli_parser = OptionParser(
        usage='usage: %prog [options] eval_code+'
        )
    cli_parser.add_option("-s", "--scheme_domain_port", dest="scheme_domain_port",
                      help="set SCHEME_DOMAIN_PORT", metavar="SCHEME_DOMAIN_PORT")

    # cli_parser.add_option("-f", "--fake", action="store_true",
    #                   dest="fake", default=False,
    #                   help="Call the command only once and exit.")
    (options, args) = cli_parser.parse_args()
    if options.scheme_domain_port:
        scheme_domain_port = options.scheme_domain_port
    if not args:
        cli_parser.print_usage()
        sys.exit()
    for arg in args:
        eval("%s"%arg)

