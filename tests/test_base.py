import sys
import os
import re

os.chdir(os.path.dirname(__file__))

sys.path.append('../applications/arabictest/modules/')
sys.path.append('../')

import helpers as h
import gluon.languages
T = gluon.languages.translator('../applications/arabictest/languages', 'en-us')
h.T = T
h.h = h
h.__dict__.update(h._update_static_globals())


#url = "http://localhost:8000/arabictest/en-us/sarfi/test/verbs/past/passive"
#url = "http://localhost:8000/arabictest/en-us/sarfi/test/verbs/present/active-inverted"
#url = "http://localhost:8000/arabictest/en-us/sarfi/test/verbs/present/passive"
#url = "http://localhost:8000/arabictest/en-us/sarfi/test/verbs/prohibitionemph/passive"
#url = "http://localhost:8000/arabictest/en-us/sarfi/test/nouns/superlativebroken-inverted"


def get_answers(url):
    _args = url.rsplit("test/", 1)[1].split("/")
    h.LANG = re.search('arabictest/(.*?)/sarfi', url).group(0)
    table, translations = h._get_translation_tables(["sarfi"] + _args)
    answers = {}
    for i, row in enumerate(table):
        if repr(row) in answers:
            answers[repr(row)]['%d'%i] = 'True'
        else:
            answers[repr(row)] = {'phrase': repr(row), 'choice%d'%i: 'True'}
    return answers

