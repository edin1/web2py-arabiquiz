## -*- coding: utf-8 -*-
import urllib
import os
import mechanize

from test_base import url, get_answers

if __name__ == "__main__":
    answers = get_answers(url)
    for i, (key, ans) in enumerate(answers.items()):
        # Submit POST request
        data = urllib.urlencode(ans)
        resp = mechanize.Request(url, data)
        #cj.add_cookie_header(resp)
        res = mechanize.urlopen(resp)
        if i < len(answers) - 1 and res.read().find("Your answer was correct!") == -1:
            raise Exception
        elif i == len(answers) - 1:
            print "After final answer"
            with file("final.html", "w") as f:
                f.write(res.read())
            from splinter import Browser
            browser = Browser()
            _url = "file:///" + os.path.abspath("final.html")
            print _url
            browser.visit(_url)
            sys.exit()
        else:
            print i
    raise Exception



# #Create a browser object and give it some optional settings.
# br = mechanize.Browser()
# print dir(br)
# #br.set_all_readonly(False)    # allow everything to be written to
# br.set_handle_robots(False)   # ignore robots
# br.set_handle_refresh(False)  # can sometimes hang without this
# br.addheaders = [('User-agent', 'Firefox')]

# # Open a webpage and inspect its contents
# response = br.open(url)
# print response.read()      # the text of the page
# response1 = br.response()  # get the response again
# print response1.read()     # can apply lxml.html.fromstring()

# # List the forms that are in the page
# for form in br.forms():
#     print "Form name:", form.name
#     print form

# # To go on the mechanize browser object must have a form selected
# br.select_form("answers")         # works when form has a name
# br.form = list(br.forms())[0]  # use when form is unnamed

