#!/usr/bin/env python

import os

def pycCleanup(directory,path):
    for filename in directory:
        if     filename[-4:] == '.pyc':
            print '- ' + filename
            os.remove(path+os.sep+filename)
        elif os.path.isdir(path+os.sep+filename):
            pycCleanup(os.listdir(path+os.sep+filename),path+os.sep+filename)

directory = os.listdir('.')
print('Deleting pyc files recursively in: '+ str(directory))
pycCleanup(directory,'.')
