## -*- coding: utf-8 -*-
#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
import re
from decimal import Decimal
from collections import defaultdict
import json
from pprint import pprint, pformat
import logging
import traceback

import helpers as h

h.setup_globals(globals())

# if request.args:
#     response.logo = h._breadcrumb(["sarfi"] + request.args)
# else:
#     response.logo = h._breadcrumb(["sarfi"])

# def scheherazade():
#     return dict()

def _mycaller(f):
    try:
        return f()
    except Exception as e:
        # raise
        if isinstance(e, HTTP):
            raise
        # else:
        logging.error("type of exception %s"%str(type(e)))
        logging.error("Superclasses of exception %s"%str(type(e).__bases__))
        logging.error('The error was\n%s'%traceback.format_exc())
        # raise
        session.results = {}
        session.dict = {}
        return SPAN(
"Uh, oh... there was an error, and your current session was cleared. Try ",
A("reloading", _href=request.env.request_uri),
" the page. If that doesn't help...", BR(),
                A("Go to the main page!", _href=URL('default', 'index')))

response._caller = _mycaller

def index():
    # raise Exception
    # print h._structure
    results = h._get_results()
    next_test = h._next_test(results)
    passed_tests = h._get_passed_tests_number(results)
    total_tests = len(h._unraveled_structure)
    _args = h._unraveled_structure[next_test - 1]
    result_table_link = SPAN(T("You can visit"), " ", A(T("your score table."), _href=URL("sarfi", "results")))
    if total_tests == passed_tests:
        body = DIV(T("Congratulations, all tests passed!"), BR(), result_table_link)
    else:
        title = DIV(h.sarfi_title,
            XML("""<p style="max-width:%dem;margin: 0 auto">"""%h.MAX_WIDTH + T("If you want to learn about Arabic morphology you can read the book:")
        +"<a href=\"" + T("http://www.sacredlearning.org/resources/fundamentals_vol1.pdf") + "\"> " + T("Fundamentals of classical Arabic") + "</a>.</p>"))

        total = SPAN(T("Total tests passed") + ": ", A("%d/%d"%(passed_tests, total_tests), _href=URL("sarfi", "results"), _class="badge"))
        body = DIV(title, h._next_test_link(), h._go_button(), BR(), total, BR(), result_table_link,
            _style="margin: 0 auto;max-width:%dem;"%h.MAX_WIDTH)
    return dict(globals(), **locals())


def results():
    root = h.structure["sarfi"][1]
    # h._sync_results_session_db()
    links = h._gen_links(root, list(),
                         lambda _list: URL("test", args=_list))
    # This is needed to move the list from the left to the center
    # links.attributes["_style"] = links.attributes["_style"].replace("margin:auto;")
    # links.attributes["_style"] += "margin:auto;"
    tests_total = len(h._unraveled_structure)
    tests_passed = h._get_passed_tests_number()
    title = SPAN(T("Total tests passed") + ": ", A("%d/%d"%(tests_passed, tests_total), _href=URL("sarfi", "results"), _class="badge"))
    return dict(globals(), **locals())

def test_old():
    import random
    if not session.dict:
        session.dict = {}
    _key = "/".join([request.function] + request.args)
    if _key not in session.dict:
        session.dict[_key] = {
            "correct": 0,
            "incorrect": 0,
            "answered": list()}

    _request_args = request.args

    _args = ["sarfi"] + _request_args
    if tuple(_args) in h._unraveled_structure_dict:
        i = h._unraveled_structure_dict[tuple(_args)]
        table, translations = h._get_translation_tables(_args)
        breadcrumb = h._breadcrumb(_args)
        test_title = h._get_test_name(_args, i)
        current_result = None
        result_date = None
        _myargs="/".join(_args)
        if auth.user_id:
            _rows = db((db.myresult.created_by==auth.user_id) & (db.myresult.args==_myargs)).select(db.myresult.ALL)
            if _rows: # there's already a result in the db
                assert len(_rows)== 1
                current_result = _rows[0].myvalue
                result_date = _rows[0].created_on
        elif _myargs in session.results:
            current_result = session.results[_myargs]

        # if current_result is not None:
        #     if current_result >= h.PASS_TRESHOLD:
        #         test_title_class = "correct"
        #     else:
        #         test_title_class = "myerror"

        form, phrase, popup_class, phrase_class = h._test_table(table, translations)

        if form is None:
            retake = A(T("Retake test"), _class="btn-info", _style="width: 7em;", _href=request.env.path_info)
            _d = session.dict[_key]
            c = _d["correct"]
            inc = _d["incorrect"]
            t = c + inc
            p = (Decimal(str(c))/Decimal(str(t))*Decimal("100")).quantize(Decimal("0.01"))
            if auth.user_id:
                _myargs="/".join(_args)
                _rows = db((db.myresult.created_by==auth.user_id) & (db.myresult.args==_myargs)).select(db.myresult.ALL)
                if _rows: # there's already a result in the db
                    assert len(_rows)== 1
                    _rows[0].update_record(myvalue=max(_rows[0].myvalue, p), created_on=request.now)
                else:
                    db.myresult.insert(args=_myargs, myvalue=p)
            else:
                if _myargs in session.results:
                    session.results[_myargs] = max(session.results[_myargs], p)
                else:
                    session.results[_myargs] = p

            buttons = [retake]
            buttons.append(" | ")
            if i < len(h._unraveled_structure) - 1:
                _next = A(BUTTON(T("Next test") , _class="btn-success", _style="width: 7em;"), _href=URL("next"))
            else:
                _next = T("No more tests")
            buttons.append(_next)
            del session.dict[_key]
            response.view = "sarfi/score.html"
            return dict(globals(), **locals())
        #else:
        correct = session.dict[_key]["correct"]
        incorrect = session.dict[_key]["incorrect"]
        remaining = len(translations) - correct - incorrect
        question_number = correct + incorrect + 1
        if popup_class != "":
            # This is for the situation when we inform the user if he
            # answered correctly or incorrectly
            question_number -= 1
        title = SPAN(SPAN(T("T%d")%(i+1) + "-" + "%d"%question_number, _class="badge"), " " + T("Pick the correct translation for") + "...")

        response.view = "sarfi/testjs.html"
        return dict(globals(), **locals())#dict(breadcrumb=breadcrumb, phrase=phrase, title=title, form=form)
    # elif len(_request_args) == 0:
    #     response.view = "default/index.html"
    #     links = h._gen_links(_type, [name])
    #     return dict(body=links, message=h._breadcrumb(["sarfi", name]))
    args = ["sarfi"] + request.args
    _structure = h.structure
    for arg in args:
        _structure = _structure[arg][1]
    response.view = "sarfi/index.html"
    # h._sync_results_session_db()
    body = h._gen_links(_structure, args[1:], lambda _list: URL("test", args=_list))
    message=h._breadcrumb(args)
    return dict(globals(), **locals())
    # raise Exception

def test():
    if not session.dict:
        session.dict = {}
    _key = "/".join([request.function] + request.args)
    if _key not in session.dict:
        session.dict[_key] = {
            "correct": 0,
            "incorrect": 0,
            "answered": list()}

    _request_args = request.args

    _args = [request.controller] + _request_args
    if tuple(_args) in h._unraveled_structure_dict:
        i = h._unraveled_structure_dict[tuple(_args)]
        if i < (len(h._unraveled_structure_dict) - 1):
            i_next = i + 1
            link_next = h._link(['test'] + h._unraveled_structure[i_next][1:])
        else:
            i_next = None
            link_next = None
        table, translations = h._get_translation_tables(_args)
        breadcrumb = h._breadcrumb(_args)
        test_title = h._get_test_name(_args, i)
        current_result = None
        result_date = None
        _myargs="/".join(_args)
        if auth.user_id:
            _rows = db((db.myresult.created_by==auth.user_id) & (db.myresult.args==_myargs)).select(db.myresult.ALL)
            if _rows: # there's already a result in the db
                assert len(_rows)== 1
                current_result = _rows[0].myvalue
                result_date = _rows[0].created_on
        elif _myargs in session.results:
            current_result = session.results[_myargs]

        check_result_link = URL("check_result", args=request.args)
        return dict(globals(), **locals())
    args = [request.controller] + request.args
    _structure = h.structure
    for arg in args:
        _structure = _structure[arg][1]
    response.view = request.controller + "/index.html"
    # h._sync_results_session_db()
    body = h._gen_links(_structure, args[1:], lambda _list: URL("test", args=_list))
    message = h._breadcrumb(args)
    return dict(globals(), **locals())
    # raise Exception

def check_result():
    # raise Exception
    p = Decimal(request.args[-1]).quantize(Decimal("0.01"))
    assert p <= Decimal("100")
    _args = [request.controller] + request.args[:-1]
    i = h._unraveled_structure_dict[tuple(_args)]
    _myargs = "/".join(_args)
    if auth.user_id:
        _rows = db((db.myresult.created_by==auth.user_id)
                   & (db.myresult.args==_myargs)).select(db.myresult.ALL)
        if _rows: # there's already a result in the db
            assert len(_rows)== 1
            max_result = max(_rows[0].myvalue, p)
            _rows[0].update_record(myvalue=max_result, created_on=request.now)
        else:
            max_result = p
            db.myresult.insert(args=_myargs, myvalue=max_result)
    else:
        if _myargs in session.results:
            max_result = max(session.results[_myargs], p)
        else:
            max_result = p
        session.results[_myargs] = max_result
    return json.dumps({#"link": h._next_test_link().xml(),
                       "url": h._next_test_url(),
                       "name": h._next_test_name(),
                       "i": h._next_test()})


def next():
    redirect(h._next_test_url())

def skip():
    _args = ["sarfi"] + request.args
    _myargs = "/".join(_args)
    result = h._get_result(_myargs)
    if result is None or result < h.PASS_TRESHOLD:
        # This essentially marks the test as skipped
        h._set_result(_myargs, Decimal("-1"))
    else:
        pass # There's no need to skip a test that's already "passed"
    redirect(h._next_test_url())

def share():
    response.view = "sarfi/share.js"
    return dict()
