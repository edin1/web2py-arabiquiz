setInterval(function(){
    $('#blink-btn-success').toggleClass('btn-success');
}, 300);

if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}

if (typeof(randomIntFromInterval) !== typeof(Function)) {
    function randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
}

if (typeof(Array.prototype.contains) !== 'function') {
    Array.prototype.contains = function (k) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === k) {
                return true;
            }
        }
        return false;
    }
}

if (typeof(Array.prototype.remove) !== 'function') {
    Array.prototype.remove = function (element) {
        for(var i = this.length - 1; i >= 0; i--) {
            if (this[i] === element) {
                this.splice(i, 1);
            }
        }
    }
}

if (typeof(random_shuffle) !== 'function') {
    function random_shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
      return array;
    }
}

if (typeof(random_choice) !== 'function') {
    function random_choice(array) {
        index = randomIntFromInterval(0, array.length - 1);
        res = array[index];
        return res;
    }
}

if (typeof(range) !== 'function') {
    function range() {
        step = 1;
        if (arguments.length === 1) {
            start = 0;
            stop = arguments[0];
        } else if (arguments.length === 2) {
            start = arguments[0];
            stop = arguments[1];
        } else if (arguments.length === 3) {
            start = arguments[0];
            stop = arguments[1];
            step = arguments[2];
        }
        var result = [];
        for (i=start;i < stop; i=i+step) {
            result.push(i);
        }
        return result;
    };
}