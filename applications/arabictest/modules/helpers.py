## -*- coding: utf-8 -*-
import random
import ast
from pprint import pprint
from collections import OrderedDict as OD
from gluon.html import *

import mycrypto

def setup_globals(d):
    globals().update(d)

    if request.uri_language:
        LANG = request.uri_language
    else:
        if 'adminLanguage' in request.cookies and not (request.cookies['adminLanguage'] is None):
            LANG = request.cookies['adminLanguage'].value
        else:
            LANG = "en-us"
        request.uri_language = LANG
    response.cookies['adminLanguage'] = LANG
    response.cookies['adminLanguage']["path"] = "/"
    d['LANG'] = LANG
    T.force(LANG)

    globals().update(_update_static_globals())

    # response.logo = A(B('web',SPAN(2),'py'),XML('&trade;&nbsp;'),
    #               _class="brand",_href="http://www.web2py.com/")
#     response.menu = [
#     (T('Home'), False, URL('default', 'index'), [])
# ]
    # response.logo = ""
    response.SHAREURL = URL(c='sarfi', f='share', vars=dict(static=URL('static','images')))

    response.logo = A(SPAN(_class="glyphicon glyphicon-home"), " ",
                      T('αrabiquiz'), "", # T('Arabic test'),
                      _href=URL('default', 'index'), _class="navbar-brand")
    # response.logo.attributes["_style"] = "color: #999999;display:block;position:relative;float:left;padding-top:10px;padding-bottom:10px;padding-left:10px;"
    # response.logo.attributes["_class"] = "nav pull-left brand"

    response.menu = ""

    response.title = T("arabiquiz - Arabic language grammar test web app")
    response.google_analytics_id = "UA-40718014-1"

    if not auth.user_id:
        if not session.results:
            session.results = {}

    if session.logged_in:
        _sync_results_session_db()
        session.logged_in = False

    request.jscode = ""

    globals().update(locals())

def _update_static_globals():
    # These are global variables that are not dependant on arabictest internals
    # sarfi_title = SPAN(T("Arabic morphology (sarf) test,"),
    #                    SPAN(" (إِمْتِحَانُ الصَّرْفِ)", _class="arabic-font", _style="font-weight: normal"),
    # _style="font-size: 24.5px;line-height: 40px;font-weight:bold;")
    sarfi_title = H1(T("Test your knowledge of Arabic!"))
    app_title = T("Arabic language test")

    BREDSEP = T(" > ")
    PASS_TRESHOLD = 80
    ANSWER_CHOICES = 3
    MAX_WIDTH = 40

    _structure = OD([
                 ("sarfi", (T("Tests"), OD([
                  # ("qurra_codes", (T("Qurra' codes"), OD([
                  #   ("minor-codes", (T("Minor codes"),)),
                  # ]))),
                  ("pronouns", (T("Pronouns"), OD([
                    ("personal", (T("Personal## pronouns"),)),
                    ("demonstrative", (T("Demonstrative## pronouns"),)),
                    ("relative", (T("Relative## pronouns"),)),
                    ("all", (T("All combined"),)),
                  ]))),
                  ("verbs1", (T("Verbs"), OD([
                   ("past", (T("The past tense verb (اَلْفِعْلُ الْمَاضِي)"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("activeneg", (T("Active negated"),)),
                    ("passiveneg", (T("Passive negated"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("present", (T("Present tense (اَلْفِعْلُ الْمُضَارِعُ فِي حَالَةِ الرَّفْعِ)"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("activeneg", (T("Active negated"),)),
                    ("passiveneg", (T("Passive negated"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("future", (T("The future tense"), OD([
                    ("activenear", (T("Active near"),)),
                    ("passivenear", (T("Passive near"),)),
                    ("activedistant", (T("Active distant"),)),
                    ("passivedistant", (T("Passive distant"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("nasb", (T("The nasb verb state (النَّصْبُ)"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("jazm", (T("The jazm verb state (الْجَزْمُ)"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("emphatic", (T("The emphatic (اَلتَّأْكِيدُ)"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("command", (T("The command (اَلْأَمْرُ)"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("commandemph", (T("The emphatic command"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("prohibition", (T("The prohibition (اَلنَّهْيُ)"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                   ("prohibitionemph", (T("The emphatic prohibition"), OD([
                    ("active", (T("Active"),)),
                    ("passive", (T("Passive"),)),
                    ("all", (T("All combined"),)),
                    ]))),
                   ]))),
                  ("nouns1", (T("Nouns"), OD([
                   # ("participleactive", (T("The active participle (اِسْمُ الْفَاعِلِ)"), OD([
                   #  ("active", (T("Active"),)),
                   #  ("passive", (T("Passive"),)),
                   #  ]))),
                    ("participleactive", (T("Active participle"),)),
                    ("participlepassive", (T("Passive participle"),)),
                    ("zarfa", (T("The noun of time and place pattern A"),)),
                    ("zarfb", (T("The noun of time and place pattern B"),)),
                    ("alata", (T("The noun of usage pattern A"),)),
                    ("alatb", (T("The noun of usage pattern B"),)),
                    ("alatc", (T("The noun of usage pattern C"),)),
                    ("superlative", (T("Superlative (sound plural)"),)),
                    ("superlativebroken", (T("Superlative (broken plural)"),)),
                    ("all", (T("All combined"),)),
                   ]))),
                  ]))),
                ])
    structure = _add_inverted(_structure)
    _unraveled_structure = _unravel(structure)

    _unraveled_structure_dict = OD()
    for _i, _args in enumerate(_unraveled_structure):
        _unraveled_structure_dict[tuple(_args)] = _i

    _unraveled_structure_dict_names = OD()
    for _i, _args in enumerate(_unraveled_structure):
        tmp = _structure
        name = ""
        for i, arg in enumerate(_args[:-1]):
            if i != 0: # Skip the "Tests" string
                name += tmp[arg][0] + BREDSEP
            try:
                tmp = tmp[arg][1]
            except:
                raise
        final = _args[-1]
        if final.endswith("-inverted"):
            final = final[:-len("-inverted")]
            name += tmp[final][0] + BREDSEP + T(" (Arabic->English)")
        else:
            name += tmp[final][0] + BREDSEP + T(" (English->Arabic)")
        _unraveled_structure_dict_names[tuple(_args)] = name
    return locals()


def _sync_results_session_db():
    # We use this only for a user that has just logged in
    if not auth.user_id or not session.logged_in:
        raise Exception
    _rows = db(db.myresult.created_by==auth.user_id).select(db.myresult.ALL)
    processed_args = []
    for _row in _rows:
        _test_no = h._unraveled_structure_dict[tuple(_row.args.split("/"))] + 1
        if _row.args in session.results:
            session.results[_row.args] = max(session.results[_row.args], _row.myvalue)
            _row.update_record(myvalue=session.results[_row.args], created_on=request.now)
        else:
            session.results[_row.args] = _row.myvalue
        processed_args.append(_row.args)
    for _row.args in session.results:
        if _row.args not in processed_args:
            db.myresult.insert(args=_row.args, myvalue=session.results[_row.args])
    del session.results

def _get_results():
    """Returns a dict {int(test_number): Decimal(test_result)}"""
    if auth.user_id:
        results = dict()
        _rows = db(db.myresult.created_by==auth.user_id).select(db.myresult.ALL)
        for _row in _rows:
            _i = h._unraveled_structure_dict[tuple(_row.args.split("/"))] + 1
            results[_i] = _row.myvalue
    else:
        results = {}
        for key, value in session.results.items():
            _i = h._unraveled_structure_dict[tuple(key.split("/"))] + 1
            results[_i] = value
    return results

def _get_result(args):
    """Returns a Decimal. args is string"""
    if auth.user_id:
        results = dict()
        _rows = db((db.myresult.created_by==auth.user_id)
                   & (db.myresult.args==args)).select(db.myresult.ALL)
        if len(_rows) == 0:
            return None
        elif len(_rows) == 1:
            return _rows[0].myvalue
        else:
            raise Exception("More than one result for args=%s"%args)
    else:
        if args in session.results:
            return session.results[args]
        else:
            return None

def _set_result(args, value):
    """args is a string, value is Decimal-like"""
    if auth.user_id:
        _rows = db((db.myresult.created_by==auth.user_id)
                   & (db.myresult.args==args)).select(db.myresult.ALL)
        if _rows: # there's already a result in the db
            assert len(_rows)== 1
            _rows[0].update_record(myvalue=max(_rows[0].myvalue, value),
                                   created_on=request.now)
        else:
            db.myresult.insert(args=args, myvalue=value)

    else:
        session.results[args] = value

def _get_passed_tests_number(results=None):
    if results is None:
        results = h._get_results()
    passed_tests = 0
    for key, value in results.items():
        if value >= h.PASS_TRESHOLD:
            passed_tests += 1
    return passed_tests


def _next_test(results=None):
    if results is None:
        results = h._get_results()
    for i in range(1, len(h._unraveled_structure)+1):
        if i in results:
            if 0 <= results[i] < h.PASS_TRESHOLD:
                return i
        else:
            return i

def _next_test_url():
    request.uri_language = T.accepted_language
    args = h._unraveled_structure[h._next_test()-1][1:]
    return URL("test", args=args)

def _next_test_link(href=None):
    if href is None:
        href = h._next_test_url()
    return DIV(T("Your next task is"), ": ", BR(), A(h._next_test_name(),
                                                     _href=href))

def _next_test_name():
    return T("Test") +\
                " %d: "%h._next_test() +\
                h._unraveled_structure_dict_names[tuple(h._unraveled_structure[h._next_test()])]

def _go_button():
    return A(H4(T("Go to") + " " + T("Test") + " %d!"%h._next_test()), _class="btn btn-success btn-large",
                                _href=h._next_test_url())

def _retake_button():
    return A(H4(T("Retake test!")), _class="btn btn-danger btn-large",
                                _href=h._next_test_url())

def _next_question():
    _key = "/".join([request.function] + request.args)
    if session.dict[_key]["phrases_remaining"]:
        text = T("Next question.")
        link = A(text, _href=request.env.request_uri)
    else: # Final question was answered
        text = T("See the result.")
        link = A(text, _href=request.env.request_uri)
        link = SPAN(T("No more questions."), " ", link)
    return link

def _add_inverted(structure):
    res = OD()
    for key, value in structure.items():
        if isinstance(value, tuple) and len(value) > 1:
            res[key] = (value[0], _add_inverted(value[1]))
        else:
            res[key] = (value[0], )
            res[key + "-inverted"] = (value[0], )
    return res


def _unravel(structure):
    res = []
    for key, value in structure.items():
        if isinstance(value, tuple) and len(value) > 1:
            for _item in _unravel(value[1]):
                tmp = [key]
                tmp.extend(_item)
                res.append(tmp)
        else:
            res.append([key])
    return res

def _get_substructure(structure, args):
    _structure = structure
    for arg in args:
        _structure = _structure[arg][1]

def _get_noun_description(index):
    from arabis.ism import noun_descriptions
    d = {"Raf3": "Nominative",
         "Nasb": "Accusative",
         "Jarr": "Genitive",
         }

    res = noun_descriptions[index][:-1]
    res[2] = d[res[2]]
    for i in range(len(res)):
        res[i] = str(T(res[i]))
    return res

def _lower(s):
    return s.decode("utf-8").lower().encode("utf-8")

def CS(span=2):
    def _CS(*args, **kwargs):
        if "_colspan" not in kwargs:
            kwargs.update({"_colspan": '%d'%span})
        if "_style" not in kwargs:
            kwargs.update({'_style': 'text-align: center; border: 1px solid black;'})
        return TD(*args, **kwargs)
    return _CS

def _generate_choices(index, phrases, translations):
    matches = []
    translation_matches = []
    non_matches = []
    phrase = phrases[index]
    for i, x in enumerate(phrases):
        if x == phrase:
            matches.append(i)
            translation_matches.append(translations[i])
    # We have to go through the list twice because
    # we need to know all the matches in advance before
    # second loop
    for i in range(len(phrases)):
        if i not in matches and translations[i] not in translation_matches:
            non_matches.append(i)
            translation_matches.append(translations[i])
    choices = [index]
    for i in range(1): # We have at most 4 suggested answers
        try:
            _choice = random.choice(non_matches)
        except IndexError:
            break
        choices.append(_choice)
        non_matches.remove(_choice)
    random.shuffle(choices)
    return choices

def _remove_current_question_data():
    _key = "/".join([request.function] + request.args)
    codetext = session.dict[_key]["codetext"]
    index = ast.literal_eval(mycrypto.w2p_decrypt(codetext))
    del session.dict[_key]["codetext"]
    del session.dict[_key]["choices"]
    try:
        session.dict[_key]["phrases_remaining"].remove(index)
    except ValueError:
        redirect(request.env.request_uri)

def _is_inverted():
    return request.args[-1].endswith("-inverted")

def _get_translation_tables(_args):
    _request_args = _args[1:] # removing "sarfi"
    i = h._unraveled_structure_dict[tuple(_args)]
    import_string = "arabis"
    for _arg in _request_args[:-1]:
        import_string += "." + _arg
    module = __import__(import_string, globals(), locals(), ['get'], -1)# + _args[2])
    _arg = _request_args[-1]
    if _arg.endswith("-inverted"):
        _arg = _arg[:-len("-inverted")]
    result = module.get((u"فعل", u"َ", u"َ"))
    if _arg == "all":
        result = _add_all_to_results(result)
    table = result[_arg][:]
    for _i in range(len(table)):
        table[_i] = table[_i]#.encode("utf-8")
    if LANG not in result["translations"]:
        _LANG = "en-us"
    else:
        _LANG = LANG

    translations = result["translations"][_LANG][result.keys().index(_arg)][:]
    # # We do this because translations are currently in encoded in utf-8 (i.e. python str)
    # # and the original Arabic phrases are in Unicode (i.e. python unicode)
    # for i, translation in enumerate(translations):
    #     translations[i] = translation.decode("utf-8")

    # translations = h._add_translations_description(translations, _request_args)

    if _args[-1].endswith("-inverted"):
        table, translations = translations, table

    # This conversion to utf-8 is needed because there are some weird
    # conversion problems with arabic when saving in unicode
    return table, translations

def _add_all_to_results(result):
    _res = OD()
    _old = result.copy()
    _all = []
    _all_translations = OD([(_lang, []) for _lang in _old["translations"]])
    for _i, _key in enumerate(_old.keys()[:-1]): # just skip translations
        _res[_key] = _old[_key]
        _all.extend(_old[_key])
        for _lang in _old["translations"]:
            _all_translations[_lang].extend(_old["translations"][_lang][_i][:])
    _random_indexes = random.sample(range(len(_all)), 15)
    _res["all"] = [_all[_i] for _i in _random_indexes]
    _res["translations"] = OD([(_lang, _value[:]) for _lang, _value in _old["translations"].items()])
    for _lang in _old["translations"]:
        _translations = [_all_translations[_lang][_i] for _i in _random_indexes]
        _res["translations"][_lang].append(_translations)
    return _res

def _add_translations_description(translations, args):
    new_translations = []
    _d = {0: T("nominative"),
          1: T("accusative"),
          2: T("genitive")}
    for _i in range(len(translations)):
        if args[0] == "nouns1":
            # We add the grammatical case of the noun to the translations
            new_translations.append(translations[i] + " [%s]"%h._lower(", ".join(h._get_noun_description(_i))))
    return new_translations

def _form(codetext, choices, translations):
    _key = "/".join([request.function] + request.args) #request.env.path_info
    choice_table = list()
    for i, choice in enumerate(choices):
        row = list()
        _tr = translations[choice]
        _class = "width100 btn-lg btn-default" # using width100 instead of btn-block because btn-block isn't adequate here
        if h._is_inverted():
            _class += " arabic-font text-right"
        else:
            _class += "  text-left"
        _button = INPUT(_type='submit', _name="%d"%choice, _value= "%d. %s"%(i+1, _tr), _class=_class)
        _form = FORM(_button, _action='', _method='post', hidden={"codetext": codetext}, _style="margin:0 0 0.1em;")#, _name="answers", _id="answers")
        row.append(TD(_form, _style="width:100%;"))
        choice_table.append(TR(*row))
    # form_elements = list()
    # form_elements.append(TABLE(choice_table,  _class="choices-table"))
    # form_elements.append(DIV(INPUT(_type='submit', _name="submit", _value=T("Submit your answer(s)!")), _style="text-align: center; margin-top:2px;"))
    #raise Exception
    return TABLE(*choice_table, _style="text-align:center;width:100%;", _name="answers", _id="answers")

def _link(_list):
    return URL(_list[0], args=_list[1:])

def _gen_links(structure, prev_list, _link, level=0, results=None):
    if results is None:
        # We cache results for subcalls
        results = h._get_results()
    links = []
    for key, value in structure.items():
        if isinstance(value, tuple) and len(value) > 1:
            links.append(TR(TH(B(T(value[0])), _colspan="3")))
            _links = _gen_links(value[1], prev_list + [key], _link, level=level+1, results=results)
            if _links and isinstance(_links[0][0], TD):
                links.append(TR(TH(B(T("Test name")), _colspan="2", _class="th-header", _style="min-width:80%;"), TH(B(T("Result")), _class="th-header", _style="max-width:10%;min-width:5%;")))

            links.extend(_links)
        else:
            if key.endswith("-inverted"):
                _direction = T(" (English->Arabic)")
            else:
                _direction = T(" (Arabic->English)")
            test_no = h._unraveled_structure_dict[tuple(["sarfi"] + prev_list + [key])] + 1
            test_link = TD(A(T("Test") + " %d "%test_no + T(value[0]) + _direction, _href=_link(prev_list + [key])), _style="text-align:left;")
            result = None
            if test_no in results:
                # print results
                result = " %d%%"%results[test_no]
                if results[test_no] >= h.PASS_TRESHOLD:
                    test_link.attributes["_class"] = "correct"
                    test_link.attributes["_colspan"] = "2"
                    test_line = TR(test_link, TD(result, _style="text-align:right;", _class="correct"))
                else:
                    # result += " (< %d%%)"%h.PASS_TRESHOLD
                    retake_style = "border-bottom-left-radius: 0px;-o-border-bottom-left-radius: 0px;-moz-border-bottom-left-radius: 0px;-webkit-border-bottom-left-radius: 0px;"
                    retake_style += "border-top-left-radius: 0px;-o-border-top-left-radius: 0px;-moz-border-top-left-radius: 0px;-webkit-border-top-left-radius: 0px;"
                    retake_style += "border-left:0 solid;"
                    retake = TD(A(T("Retake!"), _href=_link(prev_list + [key])), _class="correct", _style=retake_style)
                    test_link.attributes["_class"] = "myerror"
                    test_link.attributes["_style"] += retake_style.replace("left", "right")
                    if results[test_no] < 0:
                        result = T("Skipped")
                    test_line = TR(test_link, retake, TD(result, _style="text-align:right;", _class="myerror"))
            else:
                test_link.attributes["_colspan"] = "2"
                test_line = TR(test_link)



            links.append(test_line)
    if level == 0:
        return DIV(DIV(TABLE(links, _style="text-align:center;max-width:%dem;"%(h.MAX_WIDTH)), _class="table-result"), _style="text-align:center;display:inline-block;")
    else:
        return links

def _test_table(phrases, translations):
    _key = "/".join([request.function] + request.args) #request.env.path_info
    if "phrases_remaining" not in session.dict[_key]:
        session.dict[_key]["phrases_remaining"] = range(len(phrases))


    if request.vars["codetext"] is not None:
        codetext = request.vars["codetext"]
        index = ast.literal_eval(mycrypto.w2p_decrypt(codetext))
        # The form was submitted
        if "codetext" in session.dict[_key]:
            # If the codetext in the submitted form matches the
            # session's codetext, things are easy
            if codetext == session.dict[_key]["codetext"]:
                choices = session.dict[_key]["choices"]
            else:
                # This is in the situation where the user submits a form
                # with a phrase that doesn't match the one we have in the
                # this part is bug prone
                session.dict[_key]["codetext"] = codetext
                choices = h._generate_choices(index, phrases, translations)
                session.dict[_key]["choices"] = choices
        else:
            # This case is evaluated in the event that a user resubmits the
            # form, but server-side we don't have it in the session
            choices = h._generate_choices(index, phrases, translations)
            session.dict[_key]["codetext"] = codetext
            session.dict[_key]["choices"] = choices
        form = h._form(codetext, choices, translations)
        el = form.element(_name=str(index))
        el.attributes["_class"] += " btn-success"
        el.attributes["_id"] = "blink-btn-success"
        el.attributes["_rel"] = "popover"
        el.attributes["_data-content"] = ""
        # form.process(keepvalues=True, message_onsuccess=None)
        errors = False
        for var, value in request.vars.items():
            if var.isdigit():
                if int(var) == index:
                    pass
                else:
                    errors = True
                    #form.errors[var] = T("✘")
                    el = form.element(_name=var)
                    el.attributes["_class"] += " btn-danger"
        if str(index) not in request.vars:
            #form.errors[choice] = "Hint!"
            if index not in session.dict[_key]["answered"]:
                session.dict[_key]["incorrect"] += 1
                #form.element(_name=choice).parent.append(DIV(T("⚠"), _class = "hint button error_wrapper"))
            errors = True
        else:
            if index not in session.dict[_key]["answered"]:
                if not errors:
                    session.dict[_key]["correct"] += 1
                else: # There were incorrect checks together with the correct ones
                    session.dict[_key]["incorrect"] += 1
        session.dict[_key]["answered"].append(index)
        h._remove_current_question_data()
        if errors == True:
            #response.flash =  DIV("Wrong!", _class="myerror")
            phrase_class = ""#btn-danger"
            popup_class = "btn-danger"
        else:
            # if len(matches) > 1:
            #     _translations = " or ".join([translations[i] for i in matches])
            # else:
            #     _translations = translations[matches[0]]
            # flash = P("Bravo! The phrase ",
            #           SPAN(phrase, _class="arabic-font", _style="font-size:2em; font-weight: normal;"),
            #     " can be translated as: %s"%_translations)

            # We generate a new phrase, and new choices
            # index = random.choice(session.dict[_key]["phrases_remaining"])
            # codetext = mycrypto.w2p_encrypt(str(index))
            # choices = h._generate_choices(index, phrases, translations)
            # session.dict[_key]["codetext"] = codetext
            # session.dict[_key]["choices"] = choices

            # form = h._form(codetext, choices, translations)
            # form.process(keepvalues=True, message_onsuccess=None)
            #response.flash = DIV(flash, _class="correct")
            # request.jscode = timeout_jscode
            phrase_class = ""#btn-success"
            popup_class = "btn-success"
        #         request.jscode += """
# $(document).ready(function(){
#     var link = '<a class="btn btn-primary" href="' + location.href + '">%s</a>';
#     $('#blink-btn-success').popover({placement: 'bottom', content: link, html: true});
#     $('#blink-btn-success').popover('show');
#  });
# """%(T("Next question")+"!")
        request.jscode += """
timeout = 1500;

$(document).ready(function() {
  $('#open-popup-link').magnificPopup({
    type:'inline',
    //showCloseBtn: false,
    mainClass: 'mfp-fade',
  });
  $('#open-popup-link').magnificPopup('open');
});
$('body').click(function() {
   setTimeout(function(){
       location.href=location.href;
   }, 1000);
});
"""
        if session.dict[_key]["phrases_remaining"]:
            redirect_message = T("Going to the next question")
        else: redirect_message = T("No more questions.") + " " + T("Going to the results page")
        request.jscode +=  """
$(document).ready(function(){
 setTimeout(function () {
  $('#btn-popup').fadeOut(200, function() {
   $(this).text('%s').fadeIn(200);
    });
 }, timeout - 500); //will call the function after 2 secs.
});

"""%(redirect_message + "...")
        request.jscode +=  """
$(document).ready(function(){
 setTimeout(function () {
  location.href=location.href; //will reload the current url
 }, timeout); //will call the function after timeout time.
 });
"""
#         request.jscode += """
# $(document).ready(function() {
#  $('#test-popup').removeClass('mfp-hide');
#  $('#test-popup').animate({ opacity: 'toggle', height: 'toggle' }, "slow", easing, callback);
# });
# """


    else:
        # The form was not submitted, and we have now two options.

        # 1. There is a possibility that there is a "codetex" and choices in
        # the  session active.  This is for the case when the user
        # jus reloads the page
        if "codetext" in session.dict[_key]:
            codetext = session.dict[_key]["codetext"]
            choices = session.dict[_key]["choices"]
            index = ast.literal_eval(mycrypto.w2p_decrypt(codetext))
        # 2. This is for the case when we give the user a new question
        else:
            try:
                index = random.choice(session.dict[_key]["phrases_remaining"])
            except IndexError:
                return (None, None, "normal", "normal")
            codetext = mycrypto.w2p_encrypt(str(index))
            choices = h._generate_choices(index, phrases, translations)
            session.dict[_key]["codetext"] = codetext
            session.dict[_key]["choices"] = choices

        form = h._form(codetext, choices, translations)

        popup_class = ""
        phrase_class = ""

    return form, phrases[index], popup_class, phrase_class
    #return auth.wiki()
    #response.flash = T("Welcome to web2py!")

def _get_test_name(args, i):
    _args = args[1:]
    test_name = A(T("Test no."), " ", "%d/%d"%(i+1, len(h._unraveled_structure)),
                            _href=URL("results"))

    return test_name


def _breadcrumb(args):
    def _get_description(args):
        res = structure
        for arg in args[:-1]:
            res = res[arg][1]
        try:
            return res[args[-1]][0]
        except KeyError:
            print res.keys(), args
            raise

    _res = []
    _res.append(LI(A(T(_get_description(args[:1])), _href=URL("results"))))
    for i, arg in enumerate(args[:-1]):
        if i == 0: continue
        _res.append(LI(A(T(_get_description(args[:i+1])), _href=URL("test", args=args[1:i+1]))))
    _args = args[1:]
    if len(args) > 1:
        _description = T(_get_description(args))
        try:
            test = [" [", h._get_test_name(args, h._unraveled_structure_dict[tuple(args)]), "]"]
            if not h._is_inverted():
                _description += T(" (Arabic->English)")
            else:
                _description += T(" (English->Arabic)")
        except KeyError:
            test = []
        _res.append(LI([_description] + test, _class="active"))#, _href=URL("test", args=_args)))
    else:
        _res[0].attributes["_class"] = "active"
    return OL(_res, _class="breadcrumb")

def _current_url():
    return '%s://%s%s' % (request.env.wsgi_url_scheme, request.env.http_host,
               request.env.web2py_original_uri)