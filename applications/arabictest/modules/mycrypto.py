import gluon.contrib.aes as AES
import threading
import os
import base64

def fast_urandom16(urandom=[], locker=threading.RLock()):
    """
    this is 4x faster than calling os.urandom(16) and prevents
    the "too many files open" issue with concurrent access to os.urandom()
    """
    try:
        return urandom.pop()
    except IndexError:
        try:
            locker.acquire()
            ur = os.urandom(16 * 1024)
            urandom += [ur[i:i + 16] for i in xrange(16, 1024 * 16, 16)]
            return ur[0:16]
        finally:
            locker.release()

def pad(s, n=32, padchar=' '):
    return s + (32 - len(s) % 32) * padchar

def AES_new(key, IV=None):
    """ Returns an AES cipher object and random IV if None specified """
    if IV is None:
        IV = fast_urandom16()

    return AES.new(key, AES.MODE_CBC, IV), IV

def w2p_encrypt(data):
    key = 'asdsaddasdasdas'
    key = pad(key[:32])
    cipher, IV = AES_new(key)
    encrypted_data = IV + cipher.encrypt(pad(data))
    return base64.urlsafe_b64encode(encrypted_data)

def w2p_decrypt(data):
    key = 'asdsaddasdasdas'
    key = pad(key[:32])
    data = base64.urlsafe_b64decode(data)
    IV, data = data[:16], data[16:]
    cipher, _ = AES_new(key, IV=IV)
    data = cipher.decrypt(data)
    data = data.rstrip(' ')
    return data
