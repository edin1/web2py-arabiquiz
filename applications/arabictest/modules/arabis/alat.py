# -*- coding: utf-8 -*-
import helpers as h
import ism
import broken_plural

suffixesAC = [
    ["Masculine", [
        ["Singular", [
            ["Raf3", u"ٌ"],
            ["Nasb", u"ًا"],
            ["Jarr", u"ٍ"],
        ]],
        ["Dual", [
            ["Raf3", u"َانِ"],
            ["Nasb", u"َيْنِ"],
            ["Jarr", u"َيْنِ"],
        ]],
    ]],
]
suffixesB = [
    ["Masculine", [
        ["Singular", [
            ["Raf3", u"َةٌ"],
            ["Nasb", u"َتًا"],
            ["Jarr", u"َةٍ"],
        ]],
        ["Dual", [
            ["Raf3", u"َتَانِ"],
            ["Nasb", u"َتَيْنِ"],
            ["Jarr", u"َتَيْنِ"],
        ]],
    ]],
]
def declinate(roots):
    BaseA = h.conjugate(roots, prefix=u"مِ", fa=u"ْ", ayn=u"َ", suffix=suffixesAC)
    BaseB = h.conjugate(roots, prefix=u"مِ", fa=u"ْ", ayn=u"َ", suffix=suffixesB)
    BaseC = h.conjugate(roots, prefix=u"مِ", fa=u"ْ", ayn=u"َا", suffix=suffixesAC)

    BrokenPluralAB = h.conjugate(roots, prefix=u"مَ", fa=u"َا", ayn=u"ِ", suffix=broken_plural.suffixes)
    BrokenPluralC = h.conjugate(roots, prefix=u"مَ", fa=u"َا", ayn=u"ِي", suffix=broken_plural.suffixes)

    h.import_(BaseA, BrokenPluralAB)
    h.import_(BaseB, BrokenPluralAB)
    h.import_(BaseC, BrokenPluralC)

    BaseA = h.dictify(BaseA)
    BaseB = h.dictify(BaseB)
    BaseC = h.dictify(BaseC)
    return BaseA, BaseB, BaseC
