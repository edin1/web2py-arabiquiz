# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
import random

personal = [
    ["3rd", [
        ["Masculine", [
            ["Singular", [
                ["Raf3", "هُوَ"],
                ["Nasb", "إِيَّاهُ"],
                ]],
            ["Dual", [
                ["Raf3", "هُمَا"],
                ["Nasb", "إِيَّاهُمَا"],
                ]],
            ["Plural", [
                ["Raf3", "هُمْ"],
                ["Nasb", "إِيَّاهُمْ"],
                ]],
        ]],
        ["Feminine", [
            ["Singular", [
                ["Raf3", "هِيَ"],
                ["Nasb", "إِيَّاهَا"],
                ]],
            ["Dual", [
                ["Raf3", "هُمَا"],
                ["Nasb", "إِيَّاهُمَا"],
                ]],
            ["Plural", [
                ["Raf3", "هُنَّ"],
                ["Nasb", "إِيَّاهُنَّ"],
                ]],
        ]],
    ]],
    ["2nd", [
        ["Masculine", [
            ["Singular", [
                ["Raf3", "أَنْتَ"],
                ["Nasb", "إِيَّاكَ"],
                ]],
            ["Dual", [
                ["Raf3", "أَنْتُمَا"],
                ["Nasb", "إِيَّاكُمَا"],
                ]],
            ["Plural", [
                ["Raf3", "أَنْتُمْ"],
                ["Nasb", "إِيَّاكُمْ"],
                ]],
        ]],
        ["Feminine", [
            ["Singular", [
                ["Raf3", "أَنْتِ"],
                ["Nasb", "إِيَّاكِ"],
                ]],
            ["Dual", [
                ["Raf3", "أَنْتُمَا"],
                ["Nasb", "إِيَّاكُمَا",],
                ]],
            ["Plural", [
                ["Raf3", "أَنْتُنَّ"],
                ["Nasb", "إِيَّاكُنَّ",],
                ]],
        ]],
    ]],
    ["1st", [
        ["M. and F.", [
            ["Singular", [
                ["Raf3", "أَنَا"],
                ["Nasb", "إِيَّايَ"],
            ]],
            ["Plural", [
                ["Raf3", "نَحْنُ"],
                ["Nasb", "إِيَّانَا"],
            ]],
        ]],
    ]],
]


translations = OD([((u"فعل", u"َ", u"َ"),
      OD([
       ("en-us", [
           ["He",
            "The two of them (males)",
            "They (males)",
            "She",
            "The two of them (females)",
            "They (females)",
            "You (male)",
            "You two (males)",
            "You (males)",
            "You (female)",
            "You two (females)",
            "You (females)",
            "I",
            "We"],
           ["This (male)",
            "These two (males)",
            "These (males)",
            "This (female)",
            "These two (females)",
            "These (females)",

            "That (male)",
            "Those two (males)",
            "Those (males)",
            "That (female)",
            "Those two (females)",
            "Those (females)",
            ],
           ["The one (male) who/that...",
            "The two (males) who/that...",
            "The (males) who/that...",
            "The one (female) who/that...",
            "The two (females) who/that...",
            "The (females) who/that...",
            ],
        ]),
       ("bs", [
           ["On",
            "Njih dvojica",
            "Oni",
            "Ona",
            "Njih dvije",
            "One",
            "Ti (čovječe)",
            "Vas dvojica",
            "Vi (ljudi)",
            "Ti (ženo)",
            "Vas dvije",
            "Vi (žene)",
            "Ja",
            "Mi"],
           ["Ovaj",
            "Ova dva",
            "Ovi",
            "Ova",
            "Ove dvije",
            "Ove",

            "Taj",
            "Ta dvojica",
            "Ti (ljudi)",
            "Ta (žena)",
            "Te dvije",
            "Te (žene)",
            ],
           ["Onaj koji...",
            "Ona dvojica koji...",
            "Oni koji...",
            "Ona koja...",
            "One dvije koje...",
            "One koje...",
            ],
    ]),
   ])
  )])

# def personal():
#     return ["هُوَ",
#             "هُمَا",
#             "هُمْ",
#             "هِيَ",
#             "هُمَا",
#             "هُنَّ",
#             "أَنْتَ",
#             "أَنْتُمَا",
#             "أَنْتُمْ",
#             "أَنْتِ",
#             "أَنْتُمَا",
#             "أَنْتُنَّ",
#             "أَنَا",
#             "نَحْنُ",
#             ]

def demonstrative():
    return ["هَذَا",
            "هَذَانِ",
            "هَؤُلَاءِ",
            "هَذِهِ",
            "هَاتَانِ",
            "هَؤُلَاءِ",

            "ذَلِكَ",
            "ذَانِكَ",
            "أُؤلَئِكَ",
            "تِلْكَ",
            "تَانِكَ",
            "أُؤلَئِكَ",
            ]

def relative():
    return ["الَّذِي",
            "اللَّذَانِ",
            "الَّذِينَ",
            "الَّتِي",
            "اللَّتَانِ",
            "اللَائِي",
            ]



def get(verb):
    # print len(translations.values()[0]["en-us"])
    # We keep the verb parameter because of the automation, otherwise,
    # it's not needed for pronouns
    return OD([("personal", [line[-1] for i, line in enumerate(h.flatten(personal)) if i % 2 == 0]), # remove the nasb case
               ("demonstrative", demonstrative()),
               ("relative", relative()),
               ("translations", translations[verb]),
            ])

# def _get(verb):
#     res = OD()
#     old = _get(verb)
#     _all = []
#     _all_translations = OD([(lang, list()) for lang in old["translations"]])
#     for i, key in enumerate(old.keys()[:-1]): # just skip translations
#         res[key] = old[key]
#         _all.extend(old[key])
#         for lang in old["translations"]:
#             _all_translations[lang].extend(old["translations"][lang][i])

#     random_indexes = random.sample(range(len(_all)), 15)
#     res["all"] = [_all[i] for i in random_indexes]
#     res["translations"] = old["translations"].copy()
#     for lang in old["translations"]:
#         translations = [_all_translations[lang][i] for i in random_indexes]
#         res["translations"][lang].append(translations)
#     return res
