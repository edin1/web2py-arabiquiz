# -*- coding: utf-8 -*-

suffixes = [
    ["Masculine", [
        ["Broken Plural", [
            ["Raf3", u"ُ"],
            ["Nasb", u"َ"],
            ["Jarr", u"َ"],
        ]],
    ]],
]
