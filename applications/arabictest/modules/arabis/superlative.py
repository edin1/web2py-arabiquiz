# -*- coding: utf-8 -*-
import helpers as h
import ism
#import broken_plural

suffixes_masculine = [
    ["Masculine", [
        ["Singular", [
            ["Raf3", u"ُ"],
            ["Nasb", u"َ"],
            ["Jarr", u"َ"],
        ]],
        ["Dual", [
            ["Raf3", u"َانِ"],
            ["Nasb", u"َيْنِ"],
            ["Jarr", u"َيْنِ"],
        ]],
        ["Plural", [
            ["Raf3", u"ُونَ"],
            ["Nasb", u"ِينَ"],
            ["Jarr", u"ِينَ"],
        ]],
    ]],
]

suffixes_feminine = [
    ["Feminine", [
        ["Singular", [
            ["Raf3", u"َى"],
            ["Nasb", u"َى"],
            ["Jarr", u"َى"],
        ]],
        ["Dual", [
            ["Raf3", u"َيَانِ"],
            ["Nasb", u"َيَيْنِ"],
            ["Jarr", u"َيَيْنِ"],
        ]],
        ["Plural", [
            ["Raf3", u"َيَاتٌ"],
            ["Nasb", u"َيَاتٍ"],
            ["Jarr", u"َيَاتٍ"],
        ]],
    ]],
]

suffixes_broken_masculine = [
    ["Masculine", [
        ["Broken Plural", [
            ["Raf3", u"ُ"],
            ["Nasb", u"َ"],
            ["Jarr", u"َ"],
        ]],
    ]],
]

suffixes_broken_feminine = [
    ["Feminine", [
        ["Broken Plural", [
            ["Raf3", u"ٌ"],
            ["Nasb", u"ًا"],
            ["Jarr", u"ٍ"],
        ]],
    ]],
]

def declinate(roots):
    Base = h.conjugate(roots, prefix=u"أَ", fa=u"ْ", ayn=u"َ", suffix=suffixes_masculine)
    h.import_(Base, h.conjugate(roots, prefix=u"", fa=u"ُ", ayn=u"ْ", suffix=suffixes_feminine))
    h.import_(Base, h.conjugate(roots, prefix=u"أَ", fa=u"َا", ayn=u"ِ", suffix=suffixes_broken_masculine))
    h.import_(Base, h.conjugate(roots, prefix=u"", fa=u"ُ", ayn=u"َ", suffix=suffixes_broken_feminine))
    return h.dictify(Base)
