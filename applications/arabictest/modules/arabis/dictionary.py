# -*- coding: utf-8 -*-
from collections import OrderedDict as OD

def lower(s):
    return s.decode("utf-8").lower().encode("utf-8")

class EnDict:
    "A dict that for every key returns as the value the key itself"
    def __getitem__(self, key):
        return key


dictionary = OD([
    ("en-us", EnDict()),
    ("bs", OD([
        ("Masculine", "Muški"),
        ("Feminine", "Ženski"),

        ("Singular", "Jednina"),
        ("Dual", "Dvojina"),
        ("Plural", "Množina"),

        ("Nominative", "Nominativ"),
        ("Genitive", "Genitiv"),
        ("Accusative", "Akuzativ"),
        ]))
    ])
