# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
import ism
from ism import suffixes, Suffixes#, conjugate
import broken_plural
import zarf
import alat
import superlative as superlative_

from arabis.dictionary import dictionary, lower

translations = OD([((u"فعل", u"َ", u"َ"),
      OD([
       ("en-us", [
           ["a male doer",
            "a male doer",
            "a male doer",
            "two male doers",
            "two male doers",
            "two male doers",
            "male doers",
            "male doers",
            "male doers",
            "a female doer",
            "a female doer",
            "a female doer",
            "two female doers",
            "two female doers",
            "two female doers",
            "female doers",
            "female doers",
            "female doers"],
           ["that which is done",
            "that which is done",
            "that which is done",
            "those two which are done",
            "those two which are done",
            "those two which are done",
            "those which are done",
            "those which are done",
            "those which are done",
            "that which is done",
            "that which is done",
            "that which is done",
            "those two which are done",
            "those two which are done",
            "those two which are done",
            "those which are done",
            "those which are done",
            "those which are done"],
           ["a time or place of doing",
            "a time or place of doing",
            "a time or place of doing",
            "two times or places of doing",
            "two times or places of doing",
            "two times or places of doing",
            "times or places of doing",
            "times or places of doing",
            "times or places of doing"],
           ["a time or place of doing",
            "a time or place of doing",
            "a time or place of doing",
            "two times or places of doing",
            "two times or places of doing",
            "two times or places of doing",
            "times or places of doing",
            "times or places of doing",
            "times or places of doing"],
           ["a tool of doing",
            "a tool of doing",
            "a tool of doing",
            "two tools of doing",
            "two tools of doing",
            "two tools of doing",
            "tools of doing",
            "tools of doing",
            "tools of doing"],
           ["a tool of doing",
            "a tool of doing",
            "a tool of doing",
            "two tools of doing",
            "two tools of doing",
            "two tools of doing",
            "tools of doing",
            "tools of doing",
            "tools of doing"],
           ["a tool of doing",
            "a tool of doing",
            "a tool of doing",
            "two tools of doing",
            "two tools of doing",
            "two tools of doing",
            "tools of doing",
            "tools of doing",
            "tools of doing"],
           ["a male who does more (or the most)",
            "a male who does more (or the most)",
            "a male who does more (or the most)",
            "two males who do more (or the most)",
            "two males who do more (or the most)",
            "two males who do more (or the most)",
            "males who do more (or the most)",
            "males who do more (or the most)",
            "males who do more (or the most)",
            "a female who does more (or the most)",
            "a female who does more (or the most)",
            "a female who does more (or the most)",
            "two females who do more (or the most)",
            "two females who do more (or the most)",
            "two females who do more (or the most)",
            "females who do more (or the most)",
            "females who do more (or the most)",
            "females who do more (or the most)"],
           ["a male who does more (or the most)",
            "a male who does more (or the most)",
            "a male who does more (or the most)",
            "two males who do more (or the most)",
            "two males who do more (or the most)",
            "two males who do more (or the most)",
            "males who do more (or the most)",
            "males who do more (or the most)",
            "males who do more (or the most)",
            "a female who does more (or the most)",
            "a female who does more (or the most)",
            "a female who does more (or the most)",
            "two females who do more (or the most)",
            "two females who do more (or the most)",
            "two females who do more (or the most)",
            "females who do more (or the most)",
            "females who do more (or the most)",
            "females who do more (or the most)"],
    ]),
       ("bs", [
           ["izvršilac",
            "izvršioca",
            "izvršioca",
            "dva izvršioca",
            "dva izvršioca",
            "dva izvršioca",
            "izvršioci",
            "izvršioce",
            "izvršilaca",
            "izvršiteljka",
            "izvršiteljku",
            "izvršiteljke",
            "dvije izvšiteljke",
            "dvije izvšiteljke",
            "dvije izvšiteljke",
            "izvršiteljke",
            "izvršiteljke",
            "izvršiteljki"],
            ["urađen",
            "urađenog",
            "urađenog",
            "dva urađena",
            "dva urađena",
            "dva urađena",
            "urađeni",
            "urađene",
            "urađenih",
            "urađena",
            "urađenu",
            "urađene",
            "dvije urađene",
            "dvije urađene",
            "dvije urađene",
            "urađene",
            "urađene",
            "urađenih"],
           ["mjesto ili vrijeme rada",
            "mjesto ili vrijeme rada",
            "mjesta ili vremena rada",
            "dva mjesta ili vremena rada",
            "dva mjesta ili vremena rada",
            "dva mjesta ili vremena rada",
            "mjesta ili vremena rada",
            "mjesta ili vremena rada",
            "mjesta ili vremena rada"],
           ["mjesto ili vrijeme rada",
            "mjesto ili vrijeme rada",
            "mjesta ili vremena rada",
            "dva mjesta ili vremena rada",
            "dva mjesta ili vremena rada",
            "dva mjesta ili vremena rada",
            "mjesta ili vremena rada",
            "mjesta ili vremena rada",
            "mjesta ili vremena rada"],
           ["oruđe za rad",
            "oruđe za rad",
            "oruđa za rad",
            "dva oruđa za rad",
            "dva oruđa za rad",
            "dva oruđa za rad",
            "oruđa za rad",
            "oruđa za rad",
            "oruđa za rad"],
           ["oruđe za rad",
            "oruđe za rad",
            "oruđa za rad",
            "dva oruđa za rad",
            "dva oruđa za rad",
            "dva oruđa za rad",
            "oruđa za rad",
            "oruđa za rad",
            "oruđa za rad"],
           ["oruđe za rad",
            "oruđe za rad",
            "oruđa za rad",
            "dva oruđa za rad",
            "dva oruđa za rad",
            "dva oruđa za rad",
            "oruđa za rad",
            "oruđa za rad",
            "oruđa za rad"],
           ["onaj koji radi (naj)više",
            "onoga koji radi (naj)više",
            "onoga koji radi (naj)više",
            "dvojica koji rade (naj)više",
            "dvojicu koji rade (naj)više",
            "dvojice koji rade (naj)više",
            "oni koji rade (naj)više",
            "one koji rade (naj)više",
            "onih koji rade (naj)više",
            "ona koja radi (naj)više",
            "onu koja radi (naj)više",
            "one koja radi (naj)više",
            "dvije koje rade (naj)više",
            "dvije koje rade (naj)više",
            "dvije koje rade (naj)više",
            "one koje rade (naj)više",
            "one koje rade (naj)više",
            "onih koje rade (naj)više"],
           ["onaj koji radi (naj)više",
            "onoga koji radi (naj)više",
            "onoga koji radi (naj)više",
            "dvojica koji rade (naj)više",
            "dvojicu koji rade (naj)više",
            "dvojice koji rade (naj)više",
            "oni koji rade (naj)više",
            "one koji rade (naj)više",
            "onih koji rade (naj)više",
            "ona koja radi (naj)više",
            "onu koja radi (naj)više",
            "one koja radi (naj)više",
            "dvije koje rade (naj)više",
            "dvije koje rade (naj)više",
            "dvije koje rade (naj)više",
            "one koje rade (naj)više",
            "one koje rade (naj)više",
            "onih koje rade (naj)više"],
    ]),
   ])
  )])

def _add_translations_description(translations):
    _d = {0: "nominative",
          1: "accusative",
          2: "genitive"}
    new_translations = OD()
    for lang in translations:
        if lang == "en-us":
            new_translations[lang] = translations[lang]
        else:
            nounforms = []
            for nounform in translations[lang]:
                strings = []
                for _i in range(len(nounform)):
                    # We add the grammatical case of the noun to the translations
                    desc = _get_noun_description(_i)
                    for i in range(len(desc)):
                        desc[i] = dictionary[lang][desc[i]]
                    strings.append(nounform[_i] + " [%s]"%lower(", ".join(desc)))
                nounforms.append(strings)
            new_translations[lang] = nounforms
    return new_translations

def _get_noun_description(index):
    from arabis.ism import noun_descriptions
    d = {"Raf3": "Nominative",
         "Nasb": "Accusative",
         "Jarr": "Genitive",
         }

    res = noun_descriptions[index][:-1]
    res[2] = d[res[2]]
    return res

def participleactive(roots):
    _active = h.conjugate(roots, fa=u"َا", ayn=u"ِ", suffix=suffixes)
    return [line[-1] for line in h.flatten(_active)]

def participlepassive(roots):
    _passive = h.conjugate(roots, prefix=u"مَ", fa=u"ْ", ayn=u"ُو", suffix=suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def zarfa(roots):
    BaseA = h.conjugate(roots, prefix=u"مَ", fa=u"ْ", ayn=u"َ", suffix=suffixes)
    BrokenPlural = h.conjugate(roots, prefix=u"مَ", fa=u"َا", ayn=u"ِ", suffix=broken_plural.suffixes)

    h.import_(BaseA, BrokenPlural)
    BaseA = h.dictify(BaseA)["Masculine"]
    del BaseA["Plural"]
    return [line[-1] for line in h.flatten(h.undictify(BaseA))]

def zarfb(roots):
    BaseB = h.conjugate(roots, prefix=u"مَ", fa=u"ْ", ayn=u"ِ", suffix=suffixes)
    BrokenPlural = h.conjugate(roots, prefix=u"مَ", fa=u"َا", ayn=u"ِ", suffix=broken_plural.suffixes)

    h.import_(BaseB, BrokenPlural)
    BaseB = h.dictify(BaseB)["Masculine"]
    del BaseB["Plural"]
    return [line[-1] for line in h.flatten(h.undictify(BaseB))]

def alata(roots):
    _res = alat.declinate(roots)[0]["Masculine"]
    return [line[-1] for line in h.flatten(h.undictify(_res))]

def alatb(roots):
    _res = alat.declinate(roots)[1]["Masculine"]
    return [line[-1] for line in h.flatten(h.undictify(_res))]

def alatc(roots):
    _res = alat.declinate(roots)[2]["Masculine"]
    return [line[-1] for line in h.flatten(h.undictify(_res))]

def superlative(roots):
    _res = superlative_.declinate(roots)
    del _res["Masculine"]["Broken Plural"]
    del _res["Feminine"]["Broken Plural"]
    return [line[-1] for line in h.flatten(h.undictify(_res))]

def superlativebroken(roots):
    _res = superlative_.declinate(roots)
    del _res["Masculine"]["Plural"]
    del _res["Feminine"]["Plural"]
    return [line[-1] for line in h.flatten(h.undictify(_res))]

def get(verb):
    roots = verb[0]
    return OD([("participleactive", participleactive(roots)),
              ("participlepassive", participlepassive(roots)),
              ("zarfa", zarfa(roots)),
              ("zarfb", zarfb(roots)),
              ("alata", alata(roots)),
              ("alatb", alatb(roots)),
              ("alatc", alatc(roots)),
              ("superlative", superlative(roots)),
              ("superlativebroken", superlativebroken(roots)),
              ("translations", _add_translations_description(translations[verb])),
            ])
