# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
from present import prefixes_active, prefixes_passive
fa=u"ْ"

suffixes = [
    ["3rd", [
        ["Masculine",[
            ["Singular", u"ْ"],
            ["Dual",  u"َا"],
            ["Plural", u"ُوا"],
            ]],
        ["Feminine", [
            ["Singular", u"ْ"],
            ["Dual",  u"َا"],
            ["Plural", u"ْنَ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine",[
            ["Singular", u"ْ"],
            ["Dual",  u"َا"],
            ["Plural", u"ُوا"],
            ]],
        ["Feminine", [
            ["Singular", u"ِي"],
            ["Dual",  u"َا"],
            ["Plural", u"ْنَ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.",[
            ["Singular", u"ْ"],
            ["Plural", u"ْ"],
            ]],
        ]],
]

translations = OD([((u"فعل", u"َ", u"َ"),
              OD([
               ("en-us", [
                 ["He/It didn't do",
                  "The two of them (males) didn't do",
                  "They (males) didn't do",
                  "She/It didn't do",
                  "The two of them (females) didn't do",
                  "They (females) didn't do",
                  "You (male) didn't do",
                  "You two (males) didn't do",
                  "You (males) didn't do",
                  "You (female) didn't do",
                  "You two (females) didn't do",
                  "You (females) didn't do",
                  "I didn't do",
                  "We didn't do"],
                 ["He/It wasn't done",
                  "The two of them (males) weren't done",
                  "They (males) weren't done",
                  "She/It wasn't done",
                  "The two of them (females) weren't done",
                  "They (females) weren't done",
                  "You (male) weren't done",
                  "You two (males) weren't done",
                  "You (males) weren't done",
                  "You (female) weren't done",
                  "You two (females) weren't done",
                  "You (females) weren't done",
                  "I wasn't done",
                  "We weren't done"]
              ]),
             ("bs", [["On nije radio",
                 "Njih dvojica nisu radili",
                 "Oni nisu radili",
                 "Ona nije radila",
                 "Njih dvije nisu radile",
                 "One nisu radile",
                 "Ti nisi radio",
                 "Vas dvojica niste radili",
                 "Vi niste radili",
                 "Ti nisi radila",
                 "Vas dvije niste radile",
                 "Vi niste radile",
                 "Ja nisam radio",
                 "Mi nismo radili"],
                ["On nije rađen",
                 "Njih dvojica nisu rađeni",
                 "Oni nisu rađeni",
                 "Ona nije rađena",
                 "Njih dvije nisu rađene",
                 "One nisu rađene",
                 "Ti nisi rađen",
                 "Vas dvojica niste rađeni",
                 "Vi niste rađeni",
                 "Ti nisi rađena",
                 "Vas dvije niste rađene",
                 "Vi niste rađene",
                 "Ja nisam rađen",
                 "Mi nismo rađeni"]
              ]),
             ])
            )])



def active(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passive(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def activeneg(roots, ayn):
    return [ur"لَمْ " + word for word in active(roots, ayn)]

def passiveneg(roots):
    return [ur"لَمْ " + word for word in passive(roots)]

def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", activeneg(roots, ayn)),
              ("passive", passiveneg(roots)),
              ("translations", translations[verb]),
            ])
