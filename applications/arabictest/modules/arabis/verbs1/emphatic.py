# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
from present import prefixes_active, prefixes_passive
fa=u"ْ"

suffixes = [
    ["3rd", [
        ["Masculine",[
            ["Singular", u"َنَّ"],
            ["Dual",  u"َانِّ"],
            ["Plural", u"ُنَّ"],
            ]],
        ["Feminine", [
            ["Singular", u"َنَّ"],
            ["Dual",  u"َانِّ"],
            ["Plural", u"ْنَانِّ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine",[
            ["Singular", u"َنَّ"],
            ["Dual",  u"َانِّ"],
            ["Plural", u"ُنَّ"],
            ]],
        ["Feminine", [
            ["Singular", u"ِنَّ"],
            ["Dual",  u"َانِّ"],
            ["Plural", u"ْنَانِّ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.",[
            ["Singular", u"َنَّ"],
            ["Plural", u"َنَّ"],
            ]],
        ]],
]

translations = OD([((u"فعل", u"َ", u"َ"),
            OD([
             ("en-us", [
                [u"Verily he/it will do!",
                 u"Verily the two of them (males) will do!",
                 u"Verily they (males) will do!",
                 u"Verily she/it will do!",
                 u"Verily the two of them (females) will do!",
                 u"Verily they (females) will do!",
                 u"Verily you (male) will do!",
                 u"Verily you two (males) will do!",
                 u"Verily you (males) will do!",
                 u"Verily you (female) will do!",
                 u"Verily you two (females) will do!",
                 u"Verily you (females) will do!",
                 u"Verily I will do!",
                 u"Verily we will do!"],
                [u"Verily he/it will be done!",
                 u"Verily the two of them (males) will be done!",
                 u"Verily they (males) will be done!",
                 u"Verily she/it will be done!",
                 u"Verily the two of them (females) will be done!",
                 u"Verily they (females) will be done!",
                 u"Verily you (male) will be done!",
                 u"Verily you two (males) will be done!",
                 u"Verily you (males) will be done!",
                 u"Verily you (female) will be done!",
                 u"Verily you two (females) will be done!",
                 u"Verily you (females) will be done!",
                 u"Verily I will be done!",
                 u"Verily we will be done!"]
              ]),
              ("bs", [["Zaista će raditi!",
                 "Zaista će njih dvojica raditi!",
                 "Zaista će oni raditi!",
                 "Zaista će ona raditi!",
                 "Zaista će njih dvije raditi!",
                 "Zaista će one raditi!",
                 "Zaista ćeš (čovječe) raditi!",
                 "Zaista ćete vas dvojica raditi!",
                 "Zaista ćete (ljudi) raditi!",
                 "Zaista ćeš (ženo) raditi!",
                 "Zaista ćete vas dvije raditi!",
                 "Zaista ćete (vi žene) raditi!",
                 "Zaista ću raditi!",
                 "Zaista ćemo raditi!"],
                ["Zaista će biti urađen!",
                 "Zaista će njih dvojica biti urađeni!",
                 "Zaista će biti urađeni!",
                 "Zaista će biti urađena!",
                 "Zaista će njih dvije biti urađene!",
                 "Zaista će biti urađene!",
                 "Zaista ćeš biti urađen!",
                 "Zaista ćete vas dvojica biti urađeni!",
                 "Zaista ćete biti urađeni!",
                 "Zaista ćeš biti urađena!",
                 "Zaista ćete vas dvije biti urađene!",
                 "Zaista ćete biti urađene!",
                 "Zaista ću biti urađen!",
                 "Zaista ćemo biti urađeni!"]
              ]),
             ])
            )])



def active(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passive(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def activelam(roots, ayn):
    return [ur"لَ" + word for word in active(roots, ayn)]

def passivelam(roots):
    return [ur"لَ" + word for word in passive(roots)]

def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", activelam(roots, ayn)),
              ("passive", passivelam(roots)),
              ("translations", translations[verb]),
            ])
