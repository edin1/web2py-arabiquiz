# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h

fa=u"َ"

suffixes = [
    ["3rd", [
        ["Masculine",[
            ["Singular", u"َ"],
            ["Dual",  u"َا"],
            ["Plural", u"ُوا"],
            ]],
        ["Feminine", [
            ["Singular", u"َتْ"],
            ["Dual",  u"َتَا"],
            ["Plural", u"ْنَ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine",[
            ["Singular", u"ْتَ"],
            ["Dual",  u"ْتُمَا"],
            ["Plural", u"ْتُمْ"],
            ]],
        ["Feminine", [
            ["Singular", u"ْتِ"],
            ["Dual",  u"ْتُمَا"],
            ["Plural", u"ْتُنَّ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.",[
            ["Singular", u"ْتُ"],
            ["Plural", u"ْنَا"],
            ]],
        ]],
]

translations = OD([((u"فعل", u"َ", u"َ"),
      OD([
       ("en-us", [
        ["He/It did",
         "The two of them (males) did",
         "They (males) did",
         "She/It did",
         "The two of them (females) did",
         "They (females) did",
         "You (male) did",
         "You two (males) did",
         "You (males) did",
         "You (female) did",
         "You two (females) did",
         "You (females) did",
         "I did",
         "We did"],
        ["He/It was done",
         "The two of them (males) were done",
         "They (males) were done",
         "She/It was done",
         "The two of them (females) were done",
         "They (females) were done",
         "You (male) were done",
         "You two (males) were done",
         "You (males) were done",
         "You (female) were done",
         "You two (females) were done",
         "You (females) were done",
         "I was done",
         "We were done"],
        ["He/It didn't do",
         "The two of them (males) didn't do",
         "They (males) didn't do",
         "She/It didn't do",
         "The two of them (females) didn't do",
         "They (females) didn't do",
         "You (male) didn't do",
         "You two (males) didn't do",
         "You (males) didn't do",
         "You (female) didn't do",
         "You two (females) didn't do",
         "You (females) didn't do",
         "I didn't do",
         "We didn't do"],
        ["He/It wasn't done",
         "The two of them (males) weren't done",
         "They (males) weren't done",
         "She/It wasn't done",
         "The two of them (females) weren't done",
         "They (females) weren't done",
         "You (male) weren't done",
         "You two (males) weren't done",
         "You (males) weren't done",
         "You (female) weren't done",
         "You two (females) weren't done",
         "You (females) weren't done",
         "I wasn't done",
         "We weren't done"]
    ]),
    ("bs", [
        ["Uradio je",
         "Njih dvojica su uradili",
         "Uradili su",
         "Uradila je",
         "Njih dvije su uradile",
         "Uradile su",
         "Uradio si",
         "Vas dvojica ste uradili",
         "Uradili ste",
         "Uradila si",
         "Vas dvije ste uradile",
         "Uradile ste",
         "Uradio sam",
         "Uradili smo"],
        ["Urađen je",
         "Njih dvojica su urađeni",
         r"Urađeni su",
         "Urađena je",
         "Njih dvije su urađene",
         "Urađene su",
         "Urađen si",
         "Vas dvojica ste urađeni",
         "Urađeni ste",
         "Urađena si",
         "Vas dvije ste urađene",
         "Urađene ste",
         "Urađen/a sam",
         "Urađeni/e smo"],
        ["Nije uradio",
         "Njih dvojica nisu uradili",
         "Nisu uradili",
         "Nije uradila",
         "Njih dvije nisu uradile",
         "Nisu uradile",
         "Nisi uradio",
         "Vas dvojica niste uradili",
         "Niste uradili",
         "Nisi uradila",
         "Vas dvije niste uradile",
         "Niste uradile",
         "Nisam uradio/la",
         "Nismo uradili/le"],
        ["Nije urađen",
         "Njih dvojica nisu urađeni",
         "Nisu urađeni",
         "Nije urađena",
         "Njih dvije nisu urađene",
         "Nisu urađene",
         "Nisi urađen",
         "Vas dvojica niste urađeni",
         "Niste urađeni",
         "Nisi urađena",
         "Vas dvije niste urađene",
         "Niste urađene",
         "Nisam urađen",
         "Nismo urađeni"]
    ]),
   ])
  )])


def active(roots, ayn):
    _active = h.conjugate(roots=roots, fa=fa, ayn=ayn, suffix=suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passive(roots):
    _passive = h.conjugate(roots=roots, fa=u"ُ", ayn=u"ِ", suffix=suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def activeneg(roots, ayn):
    _active = h.conjugate(roots=roots, fa=fa, ayn=ayn, suffix=suffixes)
    return [u"مَا " + line[-1] for line in h.flatten(_active)]

def passiveneg(roots):
    _passive = h.conjugate(roots=roots, fa=u"ُ", ayn=u"ِ", suffix=suffixes)
    return [u"مَا " + line[-1] for line in h.flatten(_passive)]

def base(roots, ayn):
    return roots[0] + fa + roots[1] + ayn + roots[2]

def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", active(roots, ayn)),
              ("passive", passive(roots)),
              ("activeneg", activeneg(roots, ayn)),
              ("passiveneg", passiveneg(roots)),
              ("translations", translations[verb]),
            ])

# def get():
#     print "OK"
