# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
from present import prefixes_active, prefixes_passive
fa=u"ْ"

suffixes = [
    ["3rd", [
        ["Masculine",[
            ["Singular", u"َ"],
            ["Dual",  u"َا"],
            ["Plural", u"ُوا"],
            ]],
        ["Feminine", [
            ["Singular", u"َ"],
            ["Dual",  u"َا"],
            ["Plural", u"ْنَ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine",[
            ["Singular", u"َ"],
            ["Dual",  u"َا"],
            ["Plural", u"ُوا"],
            ]],
        ["Feminine", [
            ["Singular", u"ِي"],
            ["Dual",  u"َا"],
            ["Plural", u"ْنَ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.",[
            ["Singular", u"َ"],
            ["Plural", u"َ"],
            ]],
        ]],
]

translations = OD([((u"فعل", u"َ", u"َ"),
             OD([
              ("en-us", [
                ["He/It will never do",
                 "The two of them (males) will never do",
                 "They (males) will never do",
                 "She/It will never do",
                 "The two of them (females) will never do",
                 "They (females) will never do",
                 "You (man) will never do",
                 "You two (men) will never do",
                 "You (men) will never do",
                 "You (woman) will never do",
                 "You two (women) will never do",
                 "You (women) will never do",
                 "I will never do",
                 "We will never do"],
                ["He/It will never be done",
                 "The two of them (males) will never be done",
                 "They (males) will never be done",
                 "She/It will never be done",
                 "The two of them (females) will never be done",
                 "They (females) will never be done",
                 "You (man) will never be done",
                 "You two (men) will never be done",
                 "You (men) will never be done",
                 "You (woman) will never be done",
                 "You two (women) will never be done",
                 "You (women) will never be done",
                 "I will never be done",
                 "We will never be done"]
                 ]),
              ("bs", [
                ["On neće nikad raditi",
                 "Njih dvojica neće nikad raditi",
                 "Oni neće nikad raditi",
                 "Ona neće nikad raditi",
                 "Njih dvije neće nikad raditi",
                 "One neće nikad raditi",
                 "Ti (čovječe) nećeš nikad raditi",
                 "Vas dvojica nećete nikad raditi",
                 "Vi (ljudi) nećete nikad raditi",
                 "Ti (ženo) nećeš nikad raditi",
                 "Vas dvije nećete nikad raditi",
                 "Vi (žene) nećete nikad raditi",
                 "Ja neću nikad raditi",
                 "Mi nećemo nikad raditi"],
                ["On neće nikad biti urađen",
                 "Njih dvojica neće nikad biti urađeni",
                 "Oni neće nikad biti urađeni",
                 "Ona neće nikad biti urađena",
                 "Njih dvije neće biti nikad urađene",
                 "One neće biti nikad urađene",
                 "Ti nećeš biti nikad urađen",
                 "Vas dvojica nećete nikad biti urađeni",
                 "Vi nećete nikad biti urađeni",
                 "Ti nećeš nikad biti urađena",
                 "Vas dvije nećete nikad biti urađene",
                 "Vi nećete nikad biti urađene",
                 "Ja neću nikad biti urađen",
                 "Mi nećemo nikad biti urađeni"]
                 ]),
                ])
               )])



def active(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passive(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def activeneg(roots, ayn):
    return [ur"لَنْ " + word for word in active(roots, ayn)]

def passiveneg(roots):
    return [ur"لَنْ " + word for word in passive(roots)]

def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", activeneg(roots, ayn)),
              ("passive", passiveneg(roots)),
              ("translations", translations[verb]),
            ])
