# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
import jazm
import emphatic
fa=u"ْ"

translations = OD([((u"فعل", u"َ", u"َ"),
            OD([
             ("en-us", [
                ["He/It must not do!",
                 "The two of them (males) must not do!",
                 "They (males) must not do!",
                 "She/It must not do!",
                 "The two of them (females) must not do!",
                 "They (females) must not do!",
                 "Don't (you male) do!",
                 "Don't (you two males) do!",
                 "Don't (you males) do!",
                 "Don't (you female) do!",
                 "Don't (you two females) do!",
                 "Don't (you females) do!",
                 "I must not do!",
                 "We must not do!"],
                ["He/It must not be done!",
                 "The two of them (males) must not be done!",
                 "They (males) must not be done!",
                 "She/It must not be done!",
                 "The two of them (females) must not be done!",
                 "They (females) must not be done!",
                 "Don't (you male) be done!",
                 "Don't (you two males) be done!",
                 "Don't (you males) be done!",
                 "Don't (you female) be done!",
                 "Don't (you two females) be done!",
                 "Don't (you females) be done!",
                 "I must not be done!",
                 "We must not be done!"]
              ]),
              ("bs", [["Neka on ne radi!",
                 "Neka njih dvojica ne rade!",
                 "Neka oni ne rade!",
                 "Neka ona ne radi!",
                 "Neka njih dvije ne rade!",
                 "Neka one ne rade!",
                 "Ne radi (čovječe)!",
                 "Ne radite vas dvojica!",
                 "Ne radite (ljudi)!",
                 "Ne radi (ženo)!",
                 "Ne radite vas dvije!",
                 "Ne radite (žene)!",
                 "Neka ne radim!",
                 "Neka ne radimo!"],
                ["Neka ne bude rađen!",
                 "Neka njih dvojica ne budu rađeni!",
                 "Neka ne budu rađeni!",
                 "Neka ne bude rađena!",
                 "Neka njih dvije ne budu rađene!",
                 "Neka ne budu rađene!",
                 "Ne budi rađen!",
                 "Ne budite rađeni vas dvojica!",
                 "Ne budite rađeni!",
                 "Ne budi rađena!",
                 "Ne budite rađene vas dvije!",
                 "Ne budite rađene!",
                 "Neka ne budem rađen!",
                 "Neka ne budemo rađeni!"]
              ]),
             ])
            )])



prefixes_active = h.flatten(jazm.prefixes_active)
for list_ in prefixes_active:
    list_[-1] = u"لَا " + list_[-1]
prefixes_active = h.unflatten(prefixes_active)


prefixes_passive = h.flatten(jazm.prefixes_passive)
for list_ in prefixes_passive:
    list_[-1] = u"لَا " + list_[-1]
prefixes_passive = h.unflatten(prefixes_passive)

def active(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=jazm.suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passive(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=jazm.suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def activeemph(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=emphatic.suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passiveemph(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=emphatic.suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", active(roots, ayn)),
              ("passive", passive(roots)),
              ("translations", translations[verb]),
            ])
