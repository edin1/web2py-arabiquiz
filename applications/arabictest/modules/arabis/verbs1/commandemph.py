# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from command import activeemph, passiveemph

translations = OD([((u"فعل", u"َ", u"َ"),
            OD([
             ("en-us", [
                [u"Verily he/it must do!",
                 u"Verily the two of them (males) must do!",
                 u"Verily they (males) must do!",
                 u"Verily she/it must do!",
                 u"Verily the two of them (females) must do!",
                 u"Verily they (females) must do!",
                 u"Verily (man) do!",
                 u"Verily (you two men) do!",
                 u"Verily (men) do!",
                 u"Verily (woman) do!",
                 u"Verily (you two women) do!",
                 u"Verily (women) do!",
                 u"Verily I must do!",
                 u"Verily we must do!"],
                [u"Verily he/it must be done!",
                 u"Verily the two of them (males) must be done!",
                 u"Verily they (males) must be done!",
                 u"Verily she/it must be done!",
                 u"Verily the two of them (females) must be done!",
                 u"Verily they (females) must be done!",
                 u"Verily (man) be done!",
                 u"Verily (you two men) be done!",
                 u"Verily (you men) be done!",
                 u"Verily (woman) be done!",
                 u"Verily (you two women) be done!",
                 u"Verily (you women) be done!",
                 u"Verily I must be done!",
                 u"Verily we must be done!"]
              ]),
	      ("bs", [[r"Svakako neka on radi!",
                 r"Svakako neka njih dvojica rade!",
                 r"Svakako neka oni rade!",
                 r"Svakako neka oni radi!",
                 r"Svakako neka njih dvije rade!",
                 r"Svakako neka one rade!",
                 r"Svakako (čovječe) radi!",
                 r"Svakako radite vas dvojica!",
                 r"Svakako radite ljudi!",
                 r"Svakako radi (ženo)!",
                 r"Svakako radite vas dvije!",
                 r"Svakako radite (vi žene)!",
                 r"Svakako neka radim!",
                 r"Svakako neka radimo!"],
                [r"Svakako neka bude rađen!",
                 r"Svakako neka budu rađeni njih dvojica!",
                 r"Svakako neka budu rađeni!",
                 r"Svakako neka bude rađena!",
                 r"Svakako neka budu rađene njih dvije!",
                 r"Svakako neka budu rađene!",
                 r"Svakako budi rađen!",
                 r"Svakako budite rađeni vas dvojica!",
                 r"Svakako budite rađeni!",
                 r"Svakako budi rađena!",
                 r"Svakako budite rađene vas dvije!",
                 r"Svakako budite rađene!",
                 r"Svakako neka budem rađen!",
                 r"Svakako neka budemo rađeni!"]
              ]),
             ])
            )])


def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", activeemph(roots, ayn)),
              ("passive", passiveemph(roots)),
              ("translations", translations[verb]),
            ])
