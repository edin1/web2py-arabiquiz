# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
fa=u"ْ"

prefixes_active = [
    ["3rd", [
        ["Masculine", [
            ["Singular", u"يَ"],
            ["Dual",  u"يَ"],
            ["Plural", u"يَ"],
            ]],
        ["Feminine", [
            ["Singular", u"تَ"],
            ["Dual",  u"تَ"],
            ["Plural", u"يَ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine", [
            ["Singular", u"تَ"],
            ["Dual",  u"تَ"],
            ["Plural", u"تَ"],
            ]],
        ["Feminine", [
            ["Singular", u"تَ"],
            ["Dual",  u"تَ"],
            ["Plural", u"تَ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.", [
            ["Singular", u"أَ"],
            ["Plural", u"نَ"],
            ]],
        ]],
]

prefixes_passive = [
    ["3rd", [
        ["Masculine", [
            ["Singular", u"يُ"],
            ["Dual",  u"يُ"],
            ["Plural", u"يُ"],
            ]],
        ["Feminine", [
            ["Singular", u"تُ"],
            ["Dual",  u"تُ"],
            ["Plural", u"يُ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine",[
            ["Singular", u"تُ"],
            ["Dual",  u"تُ"],
            ["Plural", u"تُ"],
            ]],
        ["Feminine", [
            ["Singular", u"تُ"],
            ["Dual",  u"تُ"],
            ["Plural", u"تُ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.",[
            ["Singular", u"أُ"],
            ["Plural", u"نُ"],
            ]],
        ]],
]

suffixes = [
    ["3rd", [
        ["Masculine",[
            ["Singular", u"ُ"],
            ["Dual",  u"َانِ"],
            ["Plural", u"ُونَ"],
            ]],
        ["Feminine", [
            ["Singular", u"ُ"],
            ["Dual",  u"َانِ"],
            ["Plural", u"ْنَ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine",[
            ["Singular", u"ُ"],
            ["Dual",  u"َانِ"],
            ["Plural", u"ُونَ"],
            ]],
        ["Feminine", [
            ["Singular", u"ِينَ"],
            ["Dual",  u"َانِ"],
            ["Plural", u"ْنَ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.",[
            ["Singular", u"ُ"],
            ["Plural", u"ُ"],
            ]],
        ]],
]

translations = OD([((u"فعل", u"َ", u"َ"),
               OD([
                ("en-us", [
                 ["He/It does",
                  "The two of them (males) do",
                  "They (males) do",
                  "She/It does",
                  "The two of them (females) do",
                  "They (females) do",
                  "You (male) do",
                  "You two (males) do",
                  "You (males) do",
                  "You (female) do",
                  "You two (females) do",
                  "You (females) do",
                  "I do",
                  "We do"],
                 ["He/It is being done",
                  "The two of them (males) are being done",
                  "They (males) are being done",
                  "She/It is being done",
                  "The two of them (females) are being done",
                  "They (females) are being done",
                  "You (male) are being done",
                  "You two (males) are being done",
                  "You (males) are being done",
                  "You (female) are being done",
                  "You two (females) are being done",
                  "You (females) are being done",
                  "I am being done",
                  "We are being done"],
                 ["He/It doesn't do",
                  "The two (males) don't do",
                  "They (males) don't do",
                  "She/It doesn't do",
                  "The two of them (females) don't do",
                  "They (females) don't do",
                  "You (male) don't do",
                  "You two (males) don't do",
                  "You (males) don't do",
                  "You (female) don't do",
                  "You two (females) don't do",
                  "You (females) don't do",
                  "I don't do",
                  "We don't do"],
                 ["He/It isn't being done",
                  "The two (females) aren't being done",
                  "They (females) aren't being done",
                  "She/It isn't being done",
                  "The two (females) aren't being done",
                  "They (females) aren't being done",
                  "You (male) aren't being done",
                  "You two (males) aren't being done",
                  "You (males) aren't being done",
                  "You (female) aren't being done",
                  "You two (females) aren't being done",
                  "You (females) aren't being done",
                  "I am not being done",
                  "We aren't being done"]
              ]),
            ("bs", [[r"On radi",
                  r"Njih dvojica rade",
                  r"Oni rade",
                  r"Ona radi",
                  r"Njih dvije rade",
                  r"One rade",
                  r"Ti (čovječe) radiš",
                  r"Vas dvojica radite",
                  r"Vi (ljudi) radite",
                  r"Ti (ženo) radiš",
                  r"Vas dvije radite",
                  r"Vi (žene) radite",
                  r"Ja radim",
                  r"Mi radimo"],
                 [r"Na njemu se radi",
                  r"Na njima dvojici se radi",
                  r"Na njima (ljudima) se radi",
                  r"Na njoj (ženi) se radi",
                  r"Na njima dvjema se radi",
                  r"Na njima (ženama) se radi",
                  r"Na tebi (čovječe) se radi",
                  r"Na vama dvojici se radi",
                  r"Na vama (ljudima) se radi",
                  r"Na tebi (ženo) se radi",
                  r"Na vama dvjema se radi",
                  r"Na vama (ženama) se radi",
                  r"Na meni se radi",
                  r"Na nama se radi"],
                 [r"On ne radi",
                  r"Njih dvojica ne rade",
                  r"Oni ne rade",
                  r"Ona ne radi",
                  r"Njih dvije ne rade",
                  r"One ne rade",
                  r"Ti (čovječe) ne radiš",
                  r"Vas dvojica ne radite",
                  r"Vi (ljudi) ne radite",
                  r"Ti (ženo) ne radiš",
                  r"Vas dvije ne radite",
                  r"Vi (žene) ne radite",
                  r"Ja ne radim",
                  r"Mi ne radimo"],
                 ["Na njemu se ne radi",
                  r"Na njima dvojici se ne radi",
                  r"Na njima (ljudima) se ne radi",
                  r"Na njoj se ne radi",
                  r"Na njima dvjema se ne radi",
                  r"Na njima (ženama) se ne radi",
                  r"Na tebi (čovječe) se ne radi",
                  r"Na vama dvojici se ne radi",
                  r"Na vama (ljudima) se ne radi",
                  r"Na tebi (ženo) se ne radi",
                  r"Na vama dvjema se ne radi",
                  r"Na vama (ženama) se ne radi",
                  r"Na meni se ne radi",
                  r"Na nama se ne radi"]
              ]),
             ])
            )])


def active(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passive(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=suffixes)
    return [line[-1] for line in h.flatten(_passive)]


def activeneg(roots, ayn):
    return [ur"لَا " + word for word in active(roots, ayn)]

def passiveneg(roots):
    return [ur"لَا " + word for word in passive(roots)]


def activenear(roots, ayn):
    return [ur"سَ" + word for word in active(roots, ayn)]

def passivenear(roots):
    return [ur"سَ" + word for word in passive(roots)]


def activedistant(roots, ayn):
    return [ur"سَوْفَ " + word for word in active(roots, ayn)]

def passivedistant(roots):
    return [ur"سَوْفَ " + word for word in passive(roots)]

def base(roots, ayn):
    return roots[0] + fa + roots[1] + ayn + roots[2]

def get(verb):
    roots = verb[0]
    ayn = verb[2]
    return OD([("active", active(roots, ayn)),
              ("passive", passive(roots)),
              ("activeneg", activeneg(roots, ayn)),
              ("passiveneg", passiveneg(roots)),
              ("translations", translations[verb]),
            ])
