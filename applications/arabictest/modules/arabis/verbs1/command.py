# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from arabis import helpers as h
import jazm
import emphatic
fa=u"ْ"

prefixes_active = [
    ["3rd", [
        ["Masculine",[
            ["Singular", u"لِيَ"],
            ["Dual",  u"لِيَ"],
            ["Plural", u"لِيَ"],
            ]],
        ["Feminine", [
            ["Singular", u"لِتَ"],
            ["Dual",  u"لِتَ"],
            ["Plural", u"لِيَ"],
            ]],
        ]],
    ["2nd", [
        ["Masculine",[
            ["Singular", u"اِ"],
            ["Dual",  u"اِ"],
            ["Plural", u"اِ"],
            ]],
        ["Feminine", [
            ["Singular", u"اِ"],
            ["Dual",  u"اِ"],
            ["Plural", u"اِ"],
            ]],
        ]],
    ["1st", [
        ["M. and F.",[
            ["Singular", u"لِأَ"],
            ["Plural", u"لِنَ"],
            ]],
        ]],
]

translations =OD([((u"فعل", u"َ", u"َ"),
            OD([
             ("en-us", [
                ["He/It must do!",
                 "The two of them (males) must do!",
                 "They (males) must do!",
                 "She/It must do!",
                 "The two of them (females) must do!",
                 "They (females) must do!",
                 "[You man] Do!",
                 "[You two men] Do!",
                 "[You men] Do!",
                 "[You female] Do!",
                 "[You two females] Do!",
                 "[You females] Do!",
                 "I must do!",
                 "We must do!"],
                ["He/It must be done!",
                 "The two of them (males) must be done!",
                 "They (males) must be done!",
                 "She/It must be done!",
                 "The two of them (females) must be done!",
                 "They (females) must be done!",
                 "[You man] Be done!",
                 "[You two men] Be done!",
                 "[You men] Be done!",
                 "[You female] Be done!",
                 "[You two females] Be done!",
                 "[You females] Be done!",
                 "I must be done!",
                 "We must be done!"]
              ]),
              ("bs", [[r"Neka on radi!",
                 r"Neka njih dvojica rade!",
                 r"Neka oni rade!",
                 r"Neka ona radi!",
                 r"Neka njih dvije rade!",
                 r"Neka one rade!",
                 r"Radi (čovječe)!",
                 r"Radite vas dvojica!",
                 r"Radite (ljudi)!",
                 r"Radi (ženo)!",
                 r"Radite vas dvije!",
                 r"Radite (vi žene)!",
                 r"Moram raditi!",
                 r"Moramo raditi!"],
                [r"Neka bude rađen!",
                 r"Neka njih dvojica budu rađeni!",
                 r"Neka budu rađeni!",
                 r"Neka bude rađena!",
                 r"Neka njih dvije budu rađene!",
                 r"Neka budu rađene!",
                 r"Budi rađen!",
                 r"Budite rađeni vas dvojica!",
                 r"Budite rađeni!",
                 r"Budi rađena!",
                 r"Budite rađene vas dvije!",
                 r"Budite rađene!",
                 r"Moram biti rađen!",
                 r"Moramo biti rađeni!"]
              ]),
             ])
            )])


prefixes_passive = h.flatten(jazm.prefixes_passive)
for list_ in prefixes_passive:
    list_[-1] = u"لِ" + list_[-1]
prefixes_passive = h.unflatten(prefixes_passive)

def active(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=jazm.suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passive(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=jazm.suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def activeemph(roots, ayn):
    _active = h.conjugate(roots=roots, prefix=prefixes_active, fa=fa, ayn=ayn, suffix=emphatic.suffixes)
    return [line[-1] for line in h.flatten(_active)]

def passiveemph(roots):
    _passive = h.conjugate(roots=roots, prefix=prefixes_passive, fa=fa, ayn=u"َ", suffix=emphatic.suffixes)
    return [line[-1] for line in h.flatten(_passive)]

def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", active(roots, ayn)),
              ("passive", passive(roots)),
              ("translations", translations[verb]),
            ])
