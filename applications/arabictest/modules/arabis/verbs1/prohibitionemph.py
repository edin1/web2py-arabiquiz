# -*- coding: utf-8 -*-
from collections import OrderedDict as OD
from prohibition import activeemph, passiveemph

translations = OD([((u"فعل", u"َ", u"َ"),
            OD([
             ("en-us", [
                ["Verily he/it must not do!",
                 "Verily the two of them (males) must not do!",
                 "Verily they (males) must not do!",
                 "Verily she/it must not do!",
                 "Verily the two of them (females) must not do!",
                 "Verily they (females) must not do!",
                 "Verily don't (you male) do!",
                 "Verily don't (you two males) do!",
                 "Verily don't (you males) do!",
                 "Verily don't (you female) do!",
                 "Verily don't (you two females) do!",
                 "Verily don't (you females) do!",
                 "Verily I must not do!",
                 "Verily we must not do!"],
                ["Verily he/it must not be done!",
                 "Verily the two of them (males) must not be done!",
                 "Verily they (males) must not be done!",
                 "Verily she/it must not be done!",
                 "Verily the two of them (females) must not be done!",
                 "Verily they (females) must not be done!",
                 "Verily don't (you male) be done!",
                 "Verily don't (you two males) be done!",
                 "Verily don't (you males) be done!",
                 "Verily don't (you female) be done!",
                 "Verily don't (you two females) be done!",
                 "Verily don't (you females) be done!",
                 "Verily I must not be done!",
                 "Verily we must not be done!"]
              ]),
	      ("bs", [["Neka on nikako ne radi!",
                 "Neka njih dvojica nikako ne rade!",
                 "Neka oni nikako ne rade!",
                 "Neka ona nikako ne radi!",
                 "Neka njih dvije nikako ne rade!",
                 "Neka one nikako ne rade!",
                 "Nikako ne radi (čovječe)!!",
                 "Nikako ne radite vas dvojica!",
                 "Nikako ne radite (ljudi)!!",
                 "Nikako ne radi (ženo)!",
                 "Nikako ne radite vas dvije!",
                 "Nikako ne radite (žene)!",
                 "Neka nikako ne radim!",
                 "Neka nikako ne radimo!"],
                ["Neka nikako ne bude rađen!",
                 "Neka njih dvojica nikako ne budu rađeni!",
                 "Neka nikako ne budu rađeni!",
                 "Neka nikako ne bude rađena!",
                 "Neka njih dvije nikako ne budu rađene!",
                 "Neka nikako ne budu rađene!",
                 "Nikako ne budi rađen!",
                 "Nikako ne budite rađeni vas dvojica!",
                 "Nikako ne budite rađeni!",
                 "Nikako ne budi rađena!",
                 "Nikako ne budite rađene vas dvije!",
                 "Nikako ne budite rađene!",
                 "Neka nikako ne budem rađen!",
                 "Neka nikako ne budemo rađeni!"]
              ]),
             ])
            )])


def get(verb):
    roots = verb[0]
    ayn = verb[1]
    return OD([("active", activeemph(roots, ayn)),
              ("passive", passiveemph(roots)),
              ("translations", translations[verb]),
            ])
