# -*- coding: utf-8 -*-
from arabis import helpers as h

suffixes = [
    ["Masculine", [
        ["Singular", [
            ["Raf3", U"ٌ"],
            ["Nasb", U"ًا"],
            ["Jarr", U"ٍ"],
            ]],
        ["Dual", [
            ["Raf3", U"َانِ"],
            ["Nasb", U"َيْنِ"],
            ["Jarr", U"َيْنِ"],
            ]],
        ["Plural", [
            ["Raf3", U"ُونَ"],
            ["Nasb", U"ِينَ"],
            ["Jarr", U"ِينَ"],
            ]],
    ]],
    ["Feminine", [
        ["Singular", [
            ["Raf3", U"َةٌ"],
            ["Nasb", U"َتًا"],
            ["Jarr", U"َةٍ"],
            ]],
        ["Dual", [
            ["Raf3", U"َتَانِ"],
            ["Nasb", U"َتَيْنِ"],
            ["Jarr", U"َتَيْنِ"],
            ]],
        ["Plural", [
            ["Raf3", U"َاتٌ"],
            ["Nasb", U"َاتٍ"],
            ["Jarr", U"َاتٍ"],
            ]],
    ]],
]

Suffixes = h.dictify(suffixes)
from arabis.helpers import flatten
# -1 below is for removing the actual suffix
noun_descriptions = flatten(suffixes)

def conjugate(roots, prefix="", fa="", ayn="", suffix=suffixes):
    return h.dictify(h.conjugate(roots, prefix, fa, ayn, suffix))
