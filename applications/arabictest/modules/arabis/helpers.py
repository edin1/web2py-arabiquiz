# -*- coding: utf-8 -*-
from copy import deepcopy
from collections import OrderedDict as OD

def member_values(obj):
    res = []
    for name in dir(obj):
        if not name.startswith("_"):
            res.append(getattr(obj, name))
    return res

def member_names(obj):
    res = []
    for name in dir(obj):
        if not name.startswith("_"):
            res.append(name)
    return res

def members(obj):
    res = []
    for name in dir(obj):
        if not name.startswith("_"):
            res.append((name, getattr(obj, name)))
    return res

def classify(name_, list_):
    """Converts a list to a class.

    The list has to have special structure:
    list = [["name1",list1], ["name2", list2],...]
    where child lists have the same special structure.
    Specifically, the leaf elements (also lists) have
    to have the form ["name1", "key"]
    """
    new_list = []
    for name, value in list_:
        if not isinstance(name, basestring):
            print repr(name), "should be a string"
            raise Exception
        if isinstance(value, list):
            new_list.append([name, classify(name, value)])
        elif isinstance(value, basestring):
            new_list.append([name, value])
        else:
            print "Unsupported value:", repr(value)
            raise Exception
    return type(name_, (), dict(new_list))

def dictify(list_):
    """Converts a list (structured as in classify) to a dict."""
    new_list = []
    for name, value in list_:
        if not isinstance(name, basestring):
            print repr(name), "should be a string"
            raise Exception
        if isinstance(value, list):
            new_list.append([name, dictify(value)])
        elif isinstance(value, basestring):
            new_list.append([name, value])
        else:
            print "Unsupported value:", repr(value)
            raise Exception
    return OD(new_list)

def undictify(dict_):
    """Converts a OrderedDict (as returned by dictify) to a list (structured as returned by classify)."""
    new_list = []
    for name, value in dict_.items():
        if not isinstance(name, basestring):
            print repr(name), "should be a string"
            raise Exception
        if isinstance(value, OD):
            new_list.append([name, undictify(value)])
        elif isinstance(value, basestring):
            new_list.append([name, value])
        else:
            print "Unsupported value:", repr(value)
            raise Exception
    return new_list

def flatten(list_):
    """Converts list as defined in classify to a new flattened list."""
    new_list = []
    for name, value in list_:
        if not isinstance(name, basestring):
            print repr(name), "should be a string"
            raise Exception
        if isinstance(value, list):
            for _list in flatten(value):
                new_list.append([name] + _list)
        elif isinstance(value, basestring):
            new_list.append([name, value])
        else:
            print "Unsupported value:", repr(value)
            raise Exception
    return new_list

def unflatten(list_f):
    """Converts a flattened list (flatten) to a list as defined in classify."""
    if len(list_f) == 1:
        return list_f[0][0]
    NAME = None
    list_res = []
    for list_ in list_f:
        name = list_[0]
        rest = deepcopy(list_[1:])
        if NAME is None:
            NAME = name
            list_child = [rest]
        elif NAME != name:
            list_res.append([NAME, unflatten(list_child)])
            NAME = name
            list_child = [rest]
        elif NAME == name:
            list_child.append(rest)
        else:
            raise Exception
    list_res.append([NAME, unflatten(list_child)])
    return list_res

def conjugate(roots, prefix="", fa="", ayn="", suffix=""):
    if isinstance(prefix, list) and isinstance(suffix, list):
        # Both the prefix and the suffix are lists
        prefix_f = flatten(prefix)
        suffix_f = flatten(suffix)
        result_f = [list_[:-1] for list_ in prefix_f]
        if result_f != [list_[:-1] for list_ in suffix_f]:
            print "Prefix and suffix lists must have the same structure."
            raise Exception
        #else:
        for i, list_ in enumerate(prefix_f):
            result_f[i].append(list_[-1] + roots[0] + fa + roots[1] + ayn + roots[2] + suffix_f[i][-1])
        return unflatten(result_f)
    elif isinstance(prefix, list) and isinstance(suffix, basestring):
        result_f = []
        for i, list_ in enumerate(flatten(prefix)):
            result_f.append(list_[:-1] + [list_[-1] + roots[0] + fa + roots[1] + ayn + roots[2] + suffix])
        return unflatten(result_f)
    elif isinstance(prefix, basestring) and isinstance(suffix, list):
        result_f = []
        for i, list_ in enumerate(flatten(suffix)):
            result_f.append(list_[:-1] + [prefix + roots[0] + fa + roots[1] + ayn + roots[2] + list_[-1]])
        return unflatten(result_f)
    elif isinstance(prefix, basestring) and isinstance(suffix, basestring):
        return prefix + roots[0] + fa + roots[1] + ayn + roots[2] + suffix
    else:
        raise Exception

def import_(list1, list2):
    """Import the structure from list2 into list1 leaving list2 intact,
    and integrating it with the structure present in list1.

    E.g. used for combining broken plural with normal conjugations.
    """
    list1_index = {}
    for i, list_ in enumerate(list1):
        list1_index[list_[0]] = i
    for name, value in list2:
        if name not in list1_index:
            list1.append([name, deepcopy(value)])
            continue
        #else
        if isinstance(value, list):
            import_(list1[list1_index[name]][1], value)
        elif isinstance(value, basestring):
            list1[list1_index[name]][1] = value
        else:
            raise Exception

# Used for table 16.3
# Line 1
def perfect(roots, ayn):
    return conjugate(roots=roots, fa=u"َ", ayn=ayn, suffix=u"َ")

def imperfect(roots, ayn):
    return conjugate(roots=roots, prefix=u"يَ", fa=u"ْ", ayn=ayn, suffix=u"ُ")

def masdar(roots, fa=u"َ", ayn=u"ْ"):
    return conjugate(roots=roots, fa=fa, ayn=ayn, suffix=u"ًا")

def participle_active(roots, fa, ayn):
    return conjugate(roots=roots, fa=fa, ayn=ayn, suffix=u"ٌ")

# Line 2
def passive(roots):
    s = conjugate(roots=roots, fa=u"ُ", ayn=u"ِ", suffix=u"َ")
    s += u" " + conjugate(roots=roots, prefix=u"يُ", fa=u"ْ", ayn=u"َ", suffix=u"ُ")
    return s

def participle_passive(roots):
    return conjugate(roots=roots, prefix=u"مَ", fa=u"ْ", ayn=u"ُو", suffix=u"ٌ")

# Line 3
def command(roots, ayn):
    if ayn == u"ُ":
        prefix = u"اُ"
    else:
        prefix = u"اِ"
    return conjugate(roots=roots, prefix=prefix, fa=u"ْ", ayn=ayn, suffix=u"ْ")

def prohibition(roots, ayn):
    return conjugate(roots=roots, prefix=u"لَا تَ", fa=u"ْ", ayn=ayn, suffix=u"ْ")

# Line 4
def zarf(roots, ayn):
    return conjugate(roots=roots, prefix=u"مَ", fa=u"ْ", ayn=ayn, suffix=u"ٌ")

def alat(roots, ayn):
    return conjugate(roots=roots, prefix=u"مِ", fa=u"ْ", ayn=ayn, suffix=u"ٌ")

# Line 5
def superlative_m(roots):
    return conjugate(roots=roots, prefix=u"أَ", fa=u"ْ", ayn=u"َ", suffix=u"ُ")

def superlative_f(roots):
    return conjugate(roots=roots, fa=u"ُ", ayn=u"ْ", suffix=u"َى")
