# -*- coding: utf-8 -*-
import helpers as h
import ism
import broken_plural

suffixes = [
    ["Masculine", [
        ["Singular", [
            ["Raf3", u"ٌ"],
            ["Nasb", u"ًا"],
            ["Jarr", u"ٍ"],
        ]],
        ["Dual", [
            ["Raf3", u"َانِ"],
            ["Nasb", u"َيْنِ"],
            ["Jarr", u"َيْنِ"],
        ]],
    ]],
]

def declinate(roots):
    BaseA = h.conjugate(roots, prefix=u"مَ", fa=u"ْ", ayn=u"َ", suffix=suffixes)
    BaseB = h.conjugate(roots, prefix=u"مَ", fa=u"ْ", ayn=u"ِ", suffix=suffixes)

    BrokenPlural = h.conjugate(roots, prefix=u"مَ", fa=u"َا", ayn=u"ِ", suffix=broken_plural.suffixes)

    h.import_(BaseA, BrokenPlural)
    h.import_(BaseB, BrokenPlural)

    BaseA = h.dictify(BaseA)
    BaseB = h.dictify(BaseB)
    return BaseA, BaseB
